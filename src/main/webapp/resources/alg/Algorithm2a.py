# -*- coding: utf-8 -*-
"""
Created on Sat Feb 18 20:27:52 2017

@author: Adil
"""

""" Priority Class Invarian Choose"""
import FC_Score_Algo2
import numpy as np
import time, random ,sys
import argparse
import csv
from operator import itemgetter
csv.field_size_limit(922337203)
import os
dir_path = os.path.dirname(os.path.realpath(__file__))


def readData(fileName):
    with open(fileName,"r") as fp:

        data=[]
        d={}
        index=0
        #sindex=0
        for line in fp.readlines():           
            line=[int(i) for i in line.split()]            
            data.append(line)
            d[index]=index
                     
            index+=1    
 
    data=np.array(data)
    y=[]
    for j in range(len(data[0])):
        if sum(data[:,j])>=1:
            y.append(1)
        else:
            y.append(0)
  
    return y,data,d


def Alg2a(class_idF, class_invF,class_classF,dataFile,outputPath):
    """ Input files:
    1.class_id : class-name--> class-id
    2.class-invariant relation ship file
    3.class-class relation ship file, generate the class graph
    4.data matrix invatiant- testcase data matrix

    """
    # class_idF= "Data/closure4/class_id.txt" #r
    # class_invF="Data/closure4/class_invariant.txt" #r
    # class_classF="Data/closure4/classRelationShip.txt" #r
    # dataFile = "Data/closure4/closure-100.txt"#+str(k)+".txt" #r


    output = open(outputPath, "a+")
    dd = 0
    t0=time.time()
    """read class-id file, generate class-class_name dictionary"""
    class_id_dict = {}
    with open(class_idF, "r") as c_idF:
        for line in c_idF.readlines():
            e = line.replace("\n", "").split(" ")
            class_id_dict[e[1].replace("\r", "")] = int(e[0])
    N = len(class_id_dict)
    """generate class-invariant relation ship list"""
    class_invar_List = [[] for i in range(N)]
    class_numb=len(class_id_dict)
    inv_numb=0
    with open(class_invF, "r") as c_i_F:
        for index, line in enumerate(c_i_F.readlines()):
            inv_numb+=1
            if class_id_dict.has_key(line.replace("\n", "")):
                class_invar_List[class_id_dict[line.replace("\n", "")]].append(index)

    """ read class-class file genearte the class graph"""
    edges = []
    with open(class_classF, "r") as c_c_F:
        for line in c_c_F.readlines():
            e = line.replace("\n", "").split()
            if e[0] != e[1] and class_id_dict.has_key(e[0]) and class_id_dict.has_key(e[1]):
                edges.append((class_id_dict[e[0]], class_id_dict[e[1]]))

    """Generate adjcent list"""
    adjList = np.zeros((N, N))
    for e in edges:
        adjList[e[0]][e[1]] = 1
        adjList[e[1]][e[0]] = 1

    """ set the invariant and class constrains"""
    B_I = int(0.1*inv_numb)   #budget on invariant number
    B_C = int(1*class_numb) #budget on class number

    """read data file"""
    y,V,d=readData(dataFile)


    S_star = []  ### invariant set###
    C_star = []  ### class set ####

    for i, c in enumerate(class_invar_List):
        for s in S_star:
            if s in c:
                C_star.append(i)
    C_star = list(set(C_star))

    """Sort the class graph by degree"""
    sorted_degree = []
    for i, deg in enumerate(adjList):
        sorted_degree.append((i, sum(deg)))

    sorted_degree = sorted(sorted_degree, key=lambda tup: tup[1], reverse=True)

    """Strating from the highest degree class, extract invariant with highest test failure case number invariant  and add invariants to the set S_star"""
    for degree in sorted_degree:
        if len(C_star) < B_C and degree[0] not in C_star:
            C_star.append(degree[0])
            iMax_deg = 0
            temp_index = 0
            for c in class_invar_List[degree[0]]:
                if sum(V[c]) > iMax_deg:
                    iMax_deg = sum(V[c])
                    temp_index = c

             # if temp_index test case failure >1 then add to this invariant to S_star, other wise select a random invariants from the class
            if sum(V[temp_index]) != 0:
                S_star.append(temp_index)
            else:
                S_star.append(random.choice(class_invar_List[degree[0]]))


    # print dataFile,"============================"
    # print "Invariant S_star ",len(S_star), S_star
    output.write(dataFile+"============================\n")
    output.write(",".join(map(str,S_star))+"\n")

    #print len(S_star)
    output.close()
    print "Done"
    #print "Class C_Star ",len(C_star), C_star
    #print time.time()-t0,' sec....'



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=" Run with Alg2a")
    parser.add_argument('class_id')
    parser.add_argument('class_invariant')
    parser.add_argument('class_relationship')
    parser.add_argument('raw_data')
    parser.add_argument('path')
    args = parser.parse_args()
    print args.class_id
    print args.class_invariant
    print args.class_relationship
    print args.raw_data
    output_file_name = dir_path+"/Data/"+ str(time.strftime("%d%m%Y%I%M%S")) + str(random.randint(1, 10)) + "_alg2a_result.txt"
    print "Output file :" + output_file_name
    Alg2a(args.class_id, args.class_invariant, args.class_relationship, args.raw_data, output_file_name)
