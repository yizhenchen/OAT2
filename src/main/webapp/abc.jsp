<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getServerName() + ":"
			+ request.getServerPort() + path + "/";
	String basePath2 = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>WebSocket示例</title>
<script type="text/javascript" src="resource/jquery.js"></script>
</head>
<body>
	<form action="msg/login" method="post">
		用户名:
		<select name="id">
			<option value="1">张三</option>
			<option value="2">李四</option>
		</select><br>
		密码:
		<input name="password" type="text" value="123456">
		<input type="button" id="btn_analysis" value="登录">
	</form>
</body>
<script >

$("#btn_analysis").click(function(){
	 $.ajax({
		 url: "analysis2",
		 type:"GET",
		 success: function(result){
	    console.log(result);
	    webSocket(result);
	    Analysis(result)
	    }});

})

function Analysis(uuid){
	 $.ajax({
		 url: "analysis?path="+uuid+"&uid="+uuid,
		 type:"GET",
		 success: function(result){
	   	 console.log(result);
	    }});
}

function webSocket(uid){
	var path = '<%=basePath%>';
	var websocket;
	if ('WebSocket' in window) {
		websocket = new WebSocket("ws://" + path + "/ws?uid="+uid);
	} else if ('MozWebSocket' in window) {
		websocket = new MozWebSocket("ws://" + path + "/ws"+uid);
	} else {
		websocket = new SockJS("http://" + path + "/ws/sockjs"+uid);
	}
	websocket.onopen = function(event) {
		console.log("WebSocket: Connected");
		console.log(event);
	};
	websocket.onmessage = function(event) {
		var data=JSON.parse(event.data);
		console.log("WebSocket:Recieve a message",data);
		
	};
	websocket.onerror = function(event) {
		console.log("WebSocket:Error");
		console.log(event);
	};
	websocket.onclose = function(event) {
		console.log("WebSocket:Connection Closed");
		console.log(event);
	}

}
</script>
</html>
