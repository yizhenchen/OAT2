package com.invariant.analysis.controller;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.socket.TextMessage;

import com.google.gson.GsonBuilder;
import com.invariant.analysis.websocket.Message;
import com.invariant.analysis.websocket.MyWebSocketHandler;


/**
 * Handles requests for the application file upload requests
 */
@Controller
public class FileUploadController {

	 @Autowired
	   ServletContext context; 
	 @Resource
		MyWebSocketHandler handler;
	 
	private static final Logger logger = LoggerFactory
			.getLogger(FileUploadController.class);

	/**
	 * Upload single file using Spring Controller
	 */
	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
	public @ResponseBody
	String uploadFileHandler(@RequestParam("invariantMetrixFile") MultipartFile file,
			@RequestParam("callgraphFile") MultipartFile callgraphFile,
			@RequestParam("classGraphFile") MultipartFile classGraphFile		
			) {
		String output = storeFile(file,"invariantMetrix.txt");
		output+= storeFile(callgraphFile,"callgraphFile.txt");
		output+= storeFile(classGraphFile,"classGraphFile.txt");
		return output;
	}
//	
//	
	@RequestMapping(value = "/analysis", method = RequestMethod.GET)
	public @ResponseBody
	void uploadFileHandler(@RequestParam("path") String path,@RequestParam("uuid") String uuid	) {
		String[] pathArr = path.split("@");
		this.alg(pathArr[2],pathArr[1],pathArr[0],uuid);
	}

	
	@RequestMapping(value = "/analysis2", method = RequestMethod.GET)
	public @ResponseBody
	String uploadFileHandler2(HttpServletRequest request) {
		String uid = UUID.randomUUID().toString();
		request.getSession().setAttribute("uid",uid  );
		return uid;
	}
	
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public @ResponseBody
	String test() {
		return "success";
	}
	
	@RequestMapping(value = "/readFile", method = RequestMethod.GET)
	public @ResponseBody
	String ReadFile(@RequestParam("path") String FILENAME) {
		BufferedReader br = null;
		FileReader fr = null;
		StringBuilder output = new StringBuilder();

		try {

			fr = new FileReader(FILENAME);
			br = new BufferedReader(fr);

			String sCurrentLine;

			br = new BufferedReader(new FileReader(FILENAME));

			while ((sCurrentLine = br.readLine()) != null) {
				System.out.println(sCurrentLine);
				output.append(sCurrentLine);
			}
			
		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (br != null)
					br.close();

				if (fr != null)
					fr.close();

			} catch (IOException ex) {

				ex.printStackTrace();

			}

		}
		return output.toString();

	}

	
	public String storeFile(MultipartFile file, String fileName){
		
		Random rn = new Random();
		int rnum = rn.nextInt(10) + 1;
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		Date date = new Date();
		String name = dateFormat.format(date).toString()+String.valueOf(rnum)+fileName;;
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				String uploadPath = context.getRealPath("//resources")+"/alg/Data/";
				//String uploadPath = "F:\\experiment20170214\\alg\\Data\\";
				File dir = new File(uploadPath);
				if (!dir.exists())
					dir.mkdirs();
				System.out.println(uploadPath);
				File serverFile = new File(dir.getAbsolutePath()
						+ File.separator + name);
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				logger.info("Server File Location="
						+ serverFile.getAbsolutePath());

				return "@uploaded file @ " + name;
			} catch (Exception e) {
				return "@You failed to upload " + name + " => " + e.getMessage();
			}
		} else {
			return "@You failed to upload " + name+ " because the file was empty.";
		}
	}
	
	
	

	public String RunPython(String[] cmd) throws IOException, InterruptedException {
			String output = "";
		try {
			StringBuffer cmdout = new StringBuffer();
			ProcessBuilder builder = new ProcessBuilder(cmd);
			Process proc = builder.start();
			proc.waitFor();
			InputStream fis = proc.getInputStream();
			InputStream is2 = proc.getErrorStream();  
			InputStreamReader isr = new  InputStreamReader(is2,  "GBK" );  
			BufferedReader br2 = new  BufferedReader(isr);  
			String line2;  
			 while  ((line2 = br2.readLine()) !=  null ) {  
				      System.out.println(line2);  }
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
			String line = null;
			while ((line = br.readLine()) != null) {
				if(line.contains("Output file")){
					System.out.println(line);
					output+=line;
				}
				cmdout.append(line).append(System.getProperty("line.separator"));
			}
			if (cmdout.toString().isEmpty() || cmdout.toString().trim().equals("Error")) {
				return "Error";
			} else {
				return output;
			}
		} catch (Exception e) {
			return e.toString();
		}
	}
	
	public void PUSHMessage(String uuid,String text){
		
		Message msg = new Message();
		msg.setDate(new Date());
		msg.setFrom(uuid);
		msg.setTo(uuid);
		msg.setText(text);
		try {
			handler.sendMessageToUser(uuid, new TextMessage(new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create().toJson(msg)));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
	}
	
	
	public String alg(String inputclassGraph,String inputCallGraph,String invairantMextrix,String uuid){
		FileUploadController controller = new FileUploadController();
		//step 1 : Clean data and invariants metrix
		
		
		System.out.println("clas: "+inputclassGraph);
		System.out.println("call: "+inputCallGraph);
		System.out.println("invariant metrix: "+invairantMextrix);
		
		//String pythonPath="/home/yc518178/ActivePython2.7/bin/python";
		String pythonPath="python";
		
		String path = context.getRealPath("//resources")+"/alg/";
		//String path = "F:\\experiment20170214\\alg\\";
		String alg1 = "Algorithm1.py";
		String alg2 = "Algorithm2a.py";
		String alg3 = "Algorithm2b.py";
		invairantMextrix = path+"Data/"+invairantMextrix.trim();
		inputclassGraph= path+"Data/"+inputclassGraph.trim();
		inputCallGraph= path+"Data/"+inputCallGraph.trim();
		
		
		String cleanClassR="cleanClassGraph.py";
		String cleanFunctionR="cleanCallGraph.py";
		
		String classInvariant="class_invariant_relation.py";
		String classId="get_class_id.py";
		String metrix = "getInvariantsMetrix2.py";
		String programPoint = "getInvariantsMetrix.py";
		
		String functionInvariant="fun_invariant_relation.py";
		String functionId="get_functionID.py";
		
//		
//		String classGraph = "F:\\experiment20170214\\classRelationShip.txt";
//		
//		String callGraph = "F:\\experiment20170214\\CallGraph (2).txt";
		
		String tomcatPath = path;
		
//		String[] cmd ={"python","F:/experiment20170214/alg/getInvariantsMetrix2.py","F:/experiment20170214/alg/bt.csv"};

//		String invairantMextrix="F:/experiment20170214/alg/bt.csv";
		// algorithm 1 
		
		
		//get CleanClassGraph 
				String[] cmdGetCleanClassGraph ={pythonPath,path+cleanClassR,inputclassGraph.trim()};
				String classGraph="";
				
				
				try {
					classGraph=controller.RunPython(cmdGetCleanClassGraph);
					if (!classGraph.equals("Error")&&!classGraph.contains("Traceback")){
						classGraph=classGraph.substring(classGraph.indexOf(":")+1, classGraph.length());
						classGraph=classGraph.replace("\\", "/");
						classGraph=classGraph.replace("/", "\\").trim();
					}else{
						PUSHMessage(uuid,"Error:Clean class graph occur an error. "+classGraph);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					PUSHMessage(uuid,"Error:Clean class graph occur an error.");
					e.printStackTrace();
					return "error";
				}
				PUSHMessage(uuid,"1:Clean class graph finish. file name :"+classGraph);
					
		//get GetMetrix 
		String[] cmdGetMetrix ={pythonPath,path+metrix,invairantMextrix.trim()};
		String Metrix01="";
		
		
		try {
			Metrix01=controller.RunPython(cmdGetMetrix);
			if (!Metrix01.equals("Error")&&!Metrix01.contains("Traceback")){
				Metrix01=Metrix01.substring(Metrix01.indexOf(":")+1, Metrix01.length());
				Metrix01=Metrix01.replace("\\", "/");
				Metrix01=Metrix01.replace("/", "\\").trim();
			}else{
				PUSHMessage(uuid,"Error:Get invariant metrix occur an error. "+Metrix01);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			PUSHMessage(uuid,"Error:Get invariant metrix occur an error.");
			e.printStackTrace();
			return "error";
		}
		PUSHMessage(uuid,"2:Get invariant metrix finish. File name :"+Metrix01);
		
		//Metrix01="F:\\experiment20170214\\alg\\Data\\250220170946231_All01.txt";
		//get algorithm 1's result 
				String[] cmdGetAlgorithm1Re ={pythonPath,path+alg1,Metrix01.trim(),tomcatPath};
				String AlgorithmResult="";
				
//				File f = new File(Metrix01);
//				while(!f.exists()) { 
//					try {
//						System.out.print("Metrix file not found");
//						Thread.sleep(300);
//					} catch (InterruptedException e) {
//						e.printStackTrace();
//					}
//				}
				try {
					AlgorithmResult=controller.RunPython(cmdGetAlgorithm1Re);
					if (!AlgorithmResult.equals("Error")&&!AlgorithmResult.contains("Traceback")){
						AlgorithmResult=AlgorithmResult.substring(AlgorithmResult.indexOf(":")+1, AlgorithmResult.length()).replace("\\", "/");;
					}else{
						PUSHMessage(uuid,"Error:Get Algorithm 1's result occur an error."+AlgorithmResult);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					PUSHMessage(uuid,"Error:Get Algorithm 1's result occur an error.");
					e.printStackTrace();
					return "error";
				}
				PUSHMessage(uuid,"3:Get Algorithm 1's result  finish.File name $"+AlgorithmResult);
				
				//get class-invariant's full file 
				String[] cmdGetCIFull ={pythonPath,path+classInvariant,invairantMextrix.trim()};
				String classInvariantFullFile="";
				
				
				try {
					classInvariantFullFile=controller.RunPython(cmdGetCIFull);
					if (!classInvariantFullFile.equals("Error")){
						classInvariantFullFile=classInvariantFullFile.substring(classInvariantFullFile.indexOf(":")+1, classInvariantFullFile.length()).replace("\\", "/");;
					}else{
						PUSHMessage(uuid,"Error:class-invariant's full file  occur an error. "+classInvariantFullFile);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					PUSHMessage(uuid,"Error:Get  class-invariant's full file  occur an error.");
					e.printStackTrace();
					return "error";
				}
				
				PUSHMessage(uuid,"4:Get class-invariant's full file  finish.File name :"+classInvariantFullFile);
				
				
				//get class-invariant's file 
				String[] cmdGetCI ={pythonPath,path+programPoint,classInvariantFullFile.trim()};
				String classInvariantfile="";
				
				
				try {
					classInvariantfile=controller.RunPython(cmdGetCI);
					if (!classInvariantfile.equals("Error")){
						classInvariantfile=classInvariantfile.substring(classInvariantfile.indexOf(":")+1, classInvariantfile.length()).replace("\\", "/");;
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					PUSHMessage(uuid,"Error:Get  class-invariant's  file  occur an error.");
					e.printStackTrace();
					return "error";
				}
				PUSHMessage(uuid,"5:Get class-invariant  file  finish.File name :"+classInvariantfile);
				
				

				//get class-id file 
				String[] cmdGetCID ={pythonPath,path+classId,classInvariantfile,classGraph.trim()};
				String classIdfile="";
				
				
				try {
					classIdfile=controller.RunPython(cmdGetCID);
					if (!classIdfile.equals("Error")){
						classIdfile=classIdfile.substring(classIdfile.indexOf(":")+1, classIdfile.length());
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					PUSHMessage(uuid,"Error:Get  class-id's  file  occur an error.");
					e.printStackTrace();
					return "error";
				}
				
				PUSHMessage(uuid,"6:Get class-id  file  finish.File name :"+classIdfile);
				
				//get algorithm 2a's result 
				String[] cmdGetAlgorithm2aRe ={pythonPath,path+alg2,classIdfile.trim(),classInvariantfile.trim(),classGraph.trim(),Metrix01.trim(),tomcatPath};
				String Algorithm2aResult="";
				
				
				try {
					Algorithm2aResult=controller.RunPython(cmdGetAlgorithm2aRe);
					if (!AlgorithmResult.equals("Error")){
						Algorithm2aResult=Algorithm2aResult.substring(Algorithm2aResult.indexOf(":")+1, Algorithm2aResult.length()).replace("\\", "/");;
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					PUSHMessage(uuid,"Error:Get  algorithm 2a's result   occur an error.");
					e.printStackTrace();
					return "error";
				}
				PUSHMessage(uuid,"7:Get  algorithm 2a's result finish.File name $"+Algorithm2aResult);
				
				
				
				//get function invariant full 
				String[] cmdGetFunctionInvariantFull ={pythonPath,path+functionInvariant,invairantMextrix.trim()};
				String functionInvariantFull="";
				
				
				try {
					functionInvariantFull=controller.RunPython(cmdGetFunctionInvariantFull);
					if (!functionInvariantFull.equals("Error")){
						functionInvariantFull=functionInvariantFull.substring(functionInvariantFull.indexOf(":")+1, functionInvariantFull.length()).replace("\\", "/");;
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					PUSHMessage(uuid,"Error:Get function invariant full file   occur an error.");
					e.printStackTrace();
					return "error";
				}
				PUSHMessage(uuid,"8:Get function invariant full file  finish.File name :"+functionInvariantFull);
				
				

				//clean call graph
				String[] cmdGetCallGraphFull ={pythonPath,path+cleanFunctionR,functionInvariantFull.trim(),inputCallGraph.trim()};
				String callGraph="";
				
				
				try {
					callGraph=controller.RunPython(cmdGetCallGraphFull);
					if (!callGraph.equals("Error")){
						callGraph=callGraph.substring(callGraph.indexOf(":")+1, callGraph.length()).replace("\\", "/");;
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					PUSHMessage(uuid,"Error:Clean call graph  occur an error.");
					
					e.printStackTrace();
					return "error";
				}
				PUSHMessage(uuid,"9:Clean call graph finish.File name :"+callGraph);
				
				
				
				//get function invariant 
				String[] cmdGetFunctionInvariant ={pythonPath,path+programPoint,functionInvariantFull.trim()};
				String functionInvariantFile="";
				
				
				try {
					functionInvariantFile=controller.RunPython(cmdGetFunctionInvariant);
					if (!functionInvariantFile.equals("Error")){
						functionInvariantFile=functionInvariantFile.substring(functionInvariantFile.indexOf(":")+1, functionInvariantFile.length()).replace("\\", "/");;
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					PUSHMessage(uuid,"Error:Get function invariant  occur an error.");
					
					e.printStackTrace();
					return "error";
				}
				PUSHMessage(uuid,"10:Get function invariant finish.File name :"+functionInvariantFile);
				
				
				

				//get function id 
				String[] cmdGetFunctionId ={pythonPath,path+functionId,functionInvariantFile.trim()};
				String functionIdFile="";
				String functionInvariantIdFile="";
				
				
				try {
					String file=controller.RunPython(cmdGetFunctionId);
					if (!file.equals("Error")){
						functionIdFile=file.split("@")[1];
						functionInvariantIdFile=file.split("@")[3];
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					PUSHMessage(uuid,"Error:Get function id  occur an error.");
					
					e.printStackTrace();
					return "error";
				}
				PUSHMessage(uuid,"11:Get function id finish.File name :"+functionIdFile);
				
				PUSHMessage(uuid,"12:Get function id invariant finish.File name :"+functionInvariantIdFile);
				
				
				
				//get Algorithm 2b
				String[] cmdGetAlgorithm2b ={pythonPath,path+alg3,functionIdFile.trim(),functionInvariantIdFile.trim(),callGraph.trim(),Metrix01.trim(),tomcatPath};
				String algorithm2bRes="";
				
				
				try {
					algorithm2bRes=controller.RunPython(cmdGetAlgorithm2b);
					if (!algorithm2bRes.equals("Error")){
						algorithm2bRes=algorithm2bRes.substring(algorithm2bRes.indexOf(":")+1, algorithm2bRes.length()).replace("\\", "/");;
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					PUSHMessage(uuid,"Error:Get Algorithm 2b  occur an error.");
					e.printStackTrace();
					return "error";
				}
				
				PUSHMessage(uuid,"13:Get Algorithm 2b finish.File name $"+algorithm2bRes);
				
				
				return "finish all process";
	}
	
	
//	private Sessions sessions = Sessions.getInstance();
//	
//	public void websocketPUSHMessage(String message){
//		for(Session s : sessions.getSessions()){
//    		try {
//				s.getBasicRemote().sendText(message);
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//    	}
//	}
//	
	public static void main(String arg[]) throws InterruptedException{
		FileUploadController controller = new FileUploadController();
		controller.alg("F:/experiment20170214/UML.ucls", "F:/experiment20170214/callgraph.txt", "F:/experiment20170214/bt.csv","F:/experiment20170214/");

		//controller.alg("/home/yc518178/selectionAlgorithm/UML.ucls", "/home/yc518178/selectionAlgorithm/callgraph.txt", "/home/yc518178/selectionAlgorithm/bt.csv","/home/yc518178/selectionAlgorithm/");
	}
	
	
}
