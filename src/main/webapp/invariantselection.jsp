<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<!doctype html>
<%
	String path = request.getContextPath();
	String basePath = request.getServerName() + ":"
			+ request.getServerPort() + path + "/";
	String basePath2 = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<html lang="en-US">
	<head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Online Software Anomaly Detection</title>

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
       
        <!-- Google Fonts  -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,500' rel='stylesheet' type='text/css'> <!-- Body font -->
		<link href='http://fonts.googleapis.com/css?family=Lato:300,400' rel='stylesheet' type='text/css'> <!-- Navbar font -->

		<!-- Libs and Plugins CSS -->
		<link rel="stylesheet" href="resource/inc/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="resource/inc/animations/css/animate.min.css">
		<link rel="stylesheet" href="resource/inc/font-awesome/css/font-awesome.min.css"> <!-- Font Icons -->
		<link rel="stylesheet" href="resource/inc/owl-carousel/css/owl.carousel.css">
		<link rel="stylesheet" href="resource/inc/owl-carousel/css/owl.theme.css">

		<!-- Theme CSS -->
        <link rel="stylesheet" href="resource/css/reset.css">
		<link rel="stylesheet" href="resource/css/style.css">
		<link rel="stylesheet" href="resource/css/mobile.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="resource/css/skin/cool-gray.css">
        <!-- <link rel="stylesheet" href="css/skin/ice-blue.css"> -->
        <!-- <link rel="stylesheet" href="css/skin/summer-orange.css"> -->
        <!-- <link rel="stylesheet" href="css/skin/fresh-lime.css"> -->
        <!-- <link rel="stylesheet" href="css/skin/night-purple.css"> -->

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>

    <body data-spy="scroll" data-target="#main-navbar">
        <div class="page-loader"></div>  <!-- Display loading image while page loads -->
    	<div class="body">
        
            <!--========== BEGIN HEADER ==========-->
            <header id="header" class="header-main">

                <!-- Begin Navbar -->
                <nav id="main-navbar" class="navbar navbar-default navbar-fixed-top" role="navigation"> <!-- Classes: navbar-default, navbar-inverse, navbar-fixed-top, navbar-fixed-bottom, navbar-transparent. Note: If you use non-transparent navbar, set "height: 98px;" to #header -->

                  <div class="container">

                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      
                      <a class="navbar-brand page-scroll" href="/OAT">Online Software Anomaly Detection</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                           <!--  <li><a class="page-scroll" href="body">Home</a></li>
                            <li><a class="page-scroll" href="#about-section">About</a></li>
                            <li><a class="page-scroll" href="#services-section">Services</a></li>
                            <li><a class="page-scroll" href="#portfolio-section">Works</a></li>
                            <li><a class="page-scroll" href="#team-section">Team</a></li>                            
                            <li><a class="page-scroll" href="#prices-section">Prices</a></li> -->
                            <li><a class="page-scroll" href="#contact-section">Help Info</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container -->
                </nav>
                <!-- End Navbar -->

            </header>
            <!-- ========= END HEADER =========-->
            
            
            
            
        
                
                
                
                
            <!-- Begin about section -->
			<section id="upload-section" class="page bg-style1 parallax" style="padding-top: 125px;">
                <!-- Begin page header-->
                <div class="page-header-wrapper">
                    <div class="container">
                        <div class="page-header text-center " >
                            <h2>Step By Step</h2>
                            <div class="devider"></div>
                           
                        </div>
                    </div>
                </div>
                <!-- End page header-->

                <!-- Begin rotate box-1 -->
                <div class="rotate-box-1-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                               
                                    <div class="rotate-box-info">
                                        <h4>1. Get Invariant Metrix</h4>
                                        <p>In order to obtain the invariant information, or the invariant matrix, the Daikon tool is run on the program at a specific point that contains a fault and is then compared to the Daikon results of the same point without the faulty behavior. All invariant information is then stored in a CSV file.   </p>
                                         <p><img src="resource/img/invariantM.JPG" width="500px"></p>
                                         <br><br>
                                    </div>
                               
                            </div>
                           </div>
                         <div class="row">
                            <div class="col-md-12 col-sm-12">
                                    <div class="rotate-box-info">
                                      <h4>2.Get Class RelationShip</h4>
                                        <p>The Eclipse tool ObjectAid is used to obtain a class relationship diagram, contained in a UCLS file. The ObjectAid tool is found at http://www.objectaid.com/ </p>
                                         <p><img src="resource/img/class.JPG" width="500px"></p>
                                        <br><br>
                                    </div>
                                </div>
                            </div>
						  <div class="row">
                            <div class="col-md-12 col-sm-12">
                            
                                    <div class="rotate-box-info">
                                        <h4>3. Get Call Graph </h4>
                                        <p>Call Graph is a control flow graph, which represents calling relationships between functions. The Java-callgraph tool, which is available on GitHub at https://github.com/gousiosg/java-callgraph, is used to create the call graph </p>  </div>
                 					 <p><img src="resource/img/call.JPG" width="500px"></p>
                 					 <br><br>
                               </div>
                             </div> 
                           <div class="row">
                            <div class="col-md-12 col-sm-12">
                               
                                    <div class="rotate-box-info">
                                        <h4>4. Upload Files</h4>
                                        <p>Once you get these three files at hand. Upload files here.</p>
                                         <p><a href="resource/sample.zip">You can download sample here. </a></p>
                                         <p>classdaigram.ucls is the class daigram file generated by the objectaid.</p>
                                          <p>mathcallgraph.txt is the call graph file generated by the Java-callgraph tool.</p>
                                           <p>Math-100 is the Invariant Metrix .</p>
                                          <p><img src="resource/img/sample.jpg" width="500px"></p>
                                     <br><br>
								</div>
                            </div>
                            
                        </div> <!-- /.row -->
                   
   
                
                <div class="extra-space-l"></div>
        
        	<div class="row">
				 <div class="col-md-12 col-sm-12">
                          <form id="uploadfile" method="POST" action="uploadMultipleFile" enctype="multipart/form-data">
          <h4>Upload  Invariant Metrix</h4>            
       <input type="file" id="invariantMetrixFile" name="invariantMetrixFile" class="btn btn-default btn-lg" ><br /> 

        <h4>Upload  Call Graph </h4>
        <input type="file" id="callgraphFile" name="callgraphFile" class="btn btn-default btn-lg"><br /> 
        <h4>Upload  Class Graph </h4>
         <input type="file" id="classGraphFile" name="classGraphFile" class="btn btn-default btn-lg"><br /> 
     
        <input type="submit" value="Upload"  class="btn btn-default btn-lg"> 
    </form>

                </div>
             </div>   
          </section>
          <!-- End about section -->

        
              
               <section id="cta-section">
          	<div class="cta">
            	<div class="container">
                	<div class="row">
                    
                    	<div class="col-md-9">
                        	<div id="uploaded_file">
                                <h1>No file be uploaded</h1>
                             </div>
                            
                        </div>
                        
                        <div class="col-md-3">
                        	<div class="cta-btn" >
                        <a class="btn btn-default btn-lg"  id="btn_analysis" target="_blank" role="button">Start Analysis</a>
                        	</div>
                        </div>
                        
                    </div> <!-- /.row -->
                </div> <!-- /.container -->
            </div>
          </section>
             
                 <!-- Begin cta -->
          <section id="progress-section" class="page text-center" style="display:none">
            <div class="cta">
                <div class="container">
                    <div class="row">
                       
                        <div class="col-md-12">
                            <h1 id="status_title">Running....</h1>
                            <img src="resource/img/loading.gif" width="200" id="loading">
                          
                        </div>

                    </div> 
                    <div class="row">
                       
                        <div class="col-md-12">
                           <div class="progress">
    <div id="progress-bar" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
      <span class="sr-only"></span>
    </div>
  </div>
                        </div>

                    </div> 
  					<div class="row" id="result_file"></div>
                </div> <!-- /.container -->
            </div>
          </section>
          <!-- End cta -->
          
           
       
                
                

                
            <!-- Begin footer -->
            <footer class="text-off-white">
                
              

            </footer>
            <!-- End footer -->

            <a href="#" class="scrolltotop"><i class="fa fa-arrow-up"></i></a> <!-- Scroll to top button -->
                                              
        </div><!-- body ends -->
        
        
        
        
        <!-- Plugins JS -->
		<script src="resource/inc/jquery/jquery-1.11.1.min.js"></script>
		<script src="resource/inc/bootstrap/js/bootstrap.min.js"></script>
		<script src="resource/inc/owl-carousel/js/owl.carousel.min.js"></script>
		<script src="resource/inc/stellar/js/jquery.stellar.min.js"></script>
		<script src="resource/inc/animations/js/wow.min.js"></script>
        <script src="resource/inc/waypoints.min.js"></script>
		<script src="resource/inc/isotope.pkgd.min.js"></script>
		<script src="resource/inc/classie.js"></script>
		<script src="resource/inc/jquery.easing.min.js"></script>
		<script src="resource/inc/jquery.counterup.min.js"></script>
		<script src="resource/inc/smoothscroll.js"></script>

		<!-- Theme JS -->
		<script src="resource/js/theme.js"></script>
		<script src="resource/js/notify.js"></script>
        <script type="text/javascript">



        function ShowProgress(hight){
              $("#progress-bar").attr("style","width:"+hight);    
        }

	   var file1="";
	   var file2="";
	   var file3="";

        function displayProgress(data){
        	if(data==0){
        	  $("#progress-section").show();
        	  return
        	}
        	var code =data.text.split(":")[0];
        	if(code!="Error"){
        		switch(code) {
        	    case "1":
        	    	ShowProgress("8%");
        	    	$.notify(data.text, "info");
        	        break;
        	    case "2":
        	    	ShowProgress("16%");
        	    	$.notify(data.text, "info");
        	        break;
        	    case "3":
        	    	ShowProgress("24%");
        	    	$.notify(data.text, "info");
        	    	// algorithm 1's result
        	    	file1= data.text.split("$")[1]
        	    	var html="<div class='col-sm-4'> <a target='_blank' href='resource/"+file1.substr(file1.indexOf("/alg/Data/"),file1.length)+"'>"+
                    "    <img src='resource/img/file.jpg' style='width: 50%;'> <br><p><b>Algorithm 1's Result"+
                   "</b></p></a></div>";
        	    	$("#result_file").append(html);
        	        break;
        	    case "4":
        	    	ShowProgress("30%");
        	    	$.notify(data.text, "info");
        	        break;
        	    case "5":
        	    	ShowProgress("36%");
        	    	$.notify(data.text, "info");
        	        break;
        	    case "6":
        	    	ShowProgress("44%");
        	    	$.notify(data.text, "info");
        	        break;
        	    case "7":
        	    	ShowProgress("50%");
        	    	$.notify(data.text, "info");
        	    	// algorithm 2a's result
        	    	file2= data.text.split("$")[1]
        	    	var html="<div class='col-sm-4'> <a target='_blank' href='resource/"+file2.substr(file2.indexOf("/alg/Data/"),file2.length)+"'>"+
                    "    <img src='resource/img/file.jpg' style='width: 50%;'> <br><p><b>Algorithm 2a's Result"+
                   "</b></p></a></div>";
        	    	$("#result_file").append(html);
        	        break;
        	    case "8":
        	    	ShowProgress("58%");
        	    	$.notify(data.text, "info");
        	        break;
        	    case "9":
        	    	ShowProgress("66%");
        	    	$.notify(data.text, "info");
        	        break;
        	    case "10":
        	    	ShowProgress("74%");
        	    	$.notify(data.text, "info");
        	        break;
        	    case "11":
        	    	ShowProgress("82%");
        	    	$.notify(data.text, "info");
        	        break;
        	    case "12":
        	    	ShowProgress("90%");
        	    	$.notify(data.text, "info");
        	        break;
        	    case "13":
        	    	ShowProgress("100%");
        	    	$.notify(data.text, "info");
        	    	$("#status_title").html("Analysis Completed")
        	    	$("#loading").hide();
        	    	// algorithm 2b's result
        	    	file3= data.text.split("$")[1]
        	    	var html="<div class='col-sm-4'> <a target='_blank' href='resource/"+file3.substr(file3.indexOf("/alg/Data/"),file3.length)+"'>"+
                    "    <img src='resource/img/file.jpg' style='width: 50%;'> <br><p><b>Algorithm 2b's Result"+
                   "</b></p></a></div>";
        	    	$("#result_file").append(html);
        	        break;
        	    default:
        	        
        	}
        		
        	}else{
        		 $.notify(data, "warn");
        		 $("#progress-section").hide();
        	}
          }

      
        $("#btn_analysis").click(function(){
       	 $.ajax({
       		 url: "analysis2",
       		 type:"GET",
       		 success: function(result){
       	    console.log(result);
       	    webSocket(result);
       	    Analysis(result)
       	 	displayProgress(0);
       	    }});

       })

       function Analysis(uuid){
        var path = $("#hiddenInput").text().trim();
	 $.ajax({
		 url: "analysis?path="+path+"&uuid="+uuid,
		 type:"GET",
		 success: function(result){
	   	 console.log(result);
	    }});
	}

        
        

        function webSocket(uid){
        	var path = '<%=basePath%>';
        	var websocket;
        	if ('WebSocket' in window) {
        		websocket = new WebSocket("ws://" + path + "/ws?uid="+uid);
        	} else if ('MozWebSocket' in window) {
        		websocket = new MozWebSocket("ws://" + path + "/ws"+uid);
        	} else {
        		websocket = new SockJS("http://" + path + "/ws/sockjs"+uid);
        	}
        	websocket.onopen = function(event) {
        		console.log("WebSocket: Connected");
        		console.log(event);
        	};
        	websocket.onmessage = function(event) {
        		var data=JSON.parse(event.data);
        		console.log("WebSocket:Recieve a message",data);
        		displayProgress(data);
        		
        	};
        	websocket.onerror = function(event) {
        		console.log("WebSocket:Error");
        		console.log(event);
        	};
        	websocket.onclose = function(event) {
        		console.log("WebSocket:Connection Closed");
        		console.log(event);
        	}

        }

       

       $( '#uploadfile' ).submit( function( e ) {
   var flag =true
   if($("#invariantMetrixFile").val()==""){
       $.notify("Warning: Invariants Metrix Files not be provided .", "warn");
       flag =false
   }
   if($("#callgraphFile").val()==""){
        $.notify("Warning: Call graph Files not be provided .", "warn");
        flag =false
       
   }
   if($("#classGraphFile").val()==""){
       $.notify("Warning: Class graph Files not be provided .", "warn");
       flag =false
   }
   if(flag){
    $.ajax( {
      url: 'uploadFile',
      type: 'POST',
      data: new FormData( this ),
      processData: false,
      contentType: false
    }).done(function(msg){
    	
     console.log(msg);
          // msg = "asfas  asfasf asfasdf"
          if(msg.indexOf("@")==-1){
               $.notify("Warning: Class graph Files not be provided .", "error");
          }else{
              $.notify("Files uploaded ! ", "success");
             var msgArr = msg.split("@");
                  msgArr[2]
                  msgArr[4]
                  msgArr[6] 
                   $("#uploaded_file").empty()
                  var html ="<div class='row' >"+
                          "<div class='col-sm-4'>"+
                          "    <img src='resource/img/file.jpg' style='width: 90%;'> <br><p><b>file name :"+
                           msgArr[2]+
                          "</b></p></div>"+
                          "<div class='col-sm-4'>"+
                          "    <img src='resource/img/file.jpg' style='width: 90%;'><br><p><b>file name :"+
                           msgArr[4]+
                          "</div>"+
                          "<div class='col-sm-4'>"+
                          "    <img src='resource/img/file.jpg' style='width: 90%;'><br><p><b>file name :"+
                           msgArr[6]+
                          "</div>"+
                          "</div>"+
                          "<div id='hiddenInput' style='display:none'>"+
                          msgArr[2]+"@"+msgArr[4]+"@"+msgArr[6]+"</div>";


                  $("#uploaded_file").append(html)
          }
         
    });     
   }
   e.preventDefault();
 } );

       

        </script>
    </body> 
        
            
</html>
