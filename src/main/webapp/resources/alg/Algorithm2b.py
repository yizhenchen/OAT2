# -*- coding: utf-8 -*-
"""
Created on Sat Feb 18 20:52:03 2017

@author: Adil
"""

import numpy as np
import FC_Score_Algo2,time
import random,sys,csv
print sys.maxsize
csv.field_size_limit(922337203)
import os
import argparse
dir_path = os.path.dirname(os.path.realpath(__file__))


def readData(fileName):
    with open(fileName,"r") as fp:

        data=[]
        d={}
        index=0
        #sindex=0
        for line in fp.readlines():           
            line=[int(i) for i in line.split()]            
            data.append(line)
            d[index]=index
                     
            index+=1    
 
    data=np.array(data)
    y=[]
    for j in range(len(data[0])):
        if sum(data[:,j])>=1:
            y.append(1)
        else:
            y.append(0)
  
    return y,data,d


def Alg2b(fun_idF, fun_invF, fun_funF, dataFile, outputPath):

    # dataFile = "Data/closure4/closure_data.txt" #r
    # fun_idF="Data/closure4/fun_id.txt" #r
    # fun_invF="Data/closure4/funID_invariant.txt" #r
    # fun_funF="Data/closure4/fun_fun.txt"
    # dataFile = "Data/closure4/closure-100.txt"
    output=open(outputPath,"a+")

    """ Read function-id list"""
    fun_id_dict = {}
    with open(fun_idF, "r") as c_idF:
        for line in c_idF.readlines():
            e = line.replace("\n", "").replace("\r", "").split(" ")

            fun_id_dict[e[0]] = int(e[1])

    N = len(fun_id_dict) #function number




    """ Get each function invariant list """
    fun_invar_List = [[] for i in range(N)]
    with open(fun_invF, "r") as c_i_F:
        for line in c_i_F.readlines():
            terms=line.split()
            fun_invar_List[int(terms[0])].append(terms[1])

    """ Get function function relation, and generate adjcent list"""
    edges = []
    with open(fun_funF, "r") as c_c_F:
        for line in c_c_F.readlines():
            e = line.replace("\n", "").split()
            if e[0] != e[1] and fun_id_dict.has_key(e[0]) and fun_id_dict.has_key(e[1]):
                edges.append((fun_id_dict[e[0]], fun_id_dict[e[1]]))





    """Generate the call graph"""
    adjList = np.zeros((N, N))
    for e in edges:
        adjList[e[0]][e[1]] = 1
        adjList[e[1]][e[0]] = 1

    """ Read Invariant Raw data"""
    y,V,d=readData(dataFile)

    """ set the invariant and class constrains"""
    inv_numb=len(d)
    fun_numb=N
    B_I = int(0.1*inv_numb)   #budget on invariant number
    B_F = int(1*fun_numb)  #budget on function number




    S_star = []  ### invariant set###
    C_star = []  ### class set ####


    """Sort the func nodes in fucntion graph by degree"""
    sorted_degree = []
    for i, deg in enumerate(adjList):
        sorted_degree.append((i, sum(deg)))

    sorted_degree = sorted(sorted_degree, key=lambda tup: tup[1], reverse=True)

    """Strating from the highest degree function, extract invariant with highest test failure case number invariant  and add invariants to the set S_star"""
    for degree in sorted_degree:
        if len(C_star) <= B_F and degree[0] not in C_star:
            C_star.append(degree[0])
            iMax_deg = 0
            temp_index = 0
            for c in fun_invar_List[degree[0]]:
                c=int(c)
                if sum(V[c]) > iMax_deg:
                    iMax_deg = sum(V[c])
                    temp_index = c

            if sum(V[temp_index]) != 0:
                S_star.append(temp_index)
            else:
                S_star.append(random.choice(fun_invar_List[degree[0]]))
    S_star=list(set(S_star))
    # print dataFile,"==============="
    # print len(S_star)," Invariant:", S_star
    output.write(dataFile+"============================\n")
    output.write(",".join(map(str,S_star))+"\n")
    output.close()



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=" Run with Alg2a")
    parser.add_argument('fun_id')
    parser.add_argument('fun_invariant')
    parser.add_argument('fun_relationship')
    parser.add_argument('raw_data')
    parser.add_argument('path')
    args = parser.parse_args()
    print args.fun_id
    print  args.fun_invariant
    print  args.fun_relationship
    print   args.raw_data
    output_file_name = dir_path+"/Data/"+ str(time.strftime("%d%m%Y%I%M%S")) + str(random.randint(1, 10)) + "_alg2b_result.txt"
    print "Output file :" + output_file_name
    Alg2b(args.fun_id, args.fun_invariant, args.fun_relationship, args.raw_data, output_file_name)
