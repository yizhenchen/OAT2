import json
import csv
import re
import os
dir_path = os.path.dirname(os.path.realpath(__file__))
import xml.etree.ElementTree as ET
import argparse
import time, random ,sys
print sys.maxsize
csv.field_size_limit(922337203)


def getFunctionInvariant(functionInvariant,callgraph,output):
    predefined_function_inviariant = {}


    with open(functionInvariant,"r+") as files:
        reader = csv.DictReader(files)
        for row in reader:
            str = row["Program Points"]
            predefined_function_inviariant[row["Program Points"]] = row["Program Points"]

    print len(predefined_function_inviariant)
    classRelationShip=[]


    with open(callgraph,"r+") as files:
          ' as a caller , refer to number of methods'
          ' as a callee, refer to number of callers '

          for line in files:
              'line = files.read()'
              if(line.startswith("C:")):
                  'do nothing'
              else:
                  re=line.replace("M:", "").replace("(O)", "").replace("(I)", "").replace("(M)", "").replace("(S)","").replace("\\n","").replace(":", ".").split()
                  if(re[0] in predefined_function_inviariant and re[1] in predefined_function_inviariant):
                    classRelationShip.append(line.replace("M:", "").replace("(O)", "").replace("(I)", "").replace("(M)", "").replace("(S)","").replace("\\n",""))
                    #else:
                    # print line

    print len(classRelationShip)
    with open(output, "w+") as f:
        for value in classRelationShip:
            f.write(value)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Step2: Run with Alg1")
    parser.add_argument('functionInvariant')
    parser.add_argument('callgraph')
    args = parser.parse_args()
    print args.functionInvariant
    # functionInvariant="F:\\experiment20170214\\validation-st-fn-in.csv"
    # callgraph="F:\\experiment20170214\\callgraph.txt"
    output_file_name = dir_path+"/Data/" + str(time.strftime("%d%m%Y%I%M%S")) + str(random.randint(1, 10)) + "_call_graph.txt"
    print "Output file :" + output_file_name
    getFunctionInvariant(args.functionInvariant, args.callgraph,output_file_name)

