# -*- coding: utf-8 -*-
"""
Created on Thu Oct 20 00:32:25 2016

@author: Adil
"""
import csv
import sys
import time
import argparse
import random
print sys.maxsize
csv.field_size_limit(922337203)
import os
dir_path = os.path.dirname(os.path.realpath(__file__))


def getProgramPoint(input,output):
    outfile=open(output,"w")
    i=0
    l=0
    datac=csv.reader(input)
    next(datac, None)  # skip the headers
    for d in datac:
         i=i+1
         outfile.write(d[1] + "\n")
    outfile.close()
    print("Done")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Step1: Clean up training data,only get 0,1 value")
    parser.add_argument('csv', help='csv file to Caculate', type=argparse.FileType('r'))
    args = parser.parse_args()
    print args.csv
    output_file_name = dir_path +"/Data/"+str(time.strftime("%d%m%Y%I%M%S"))+str(random.randint(1, 10))+"_All01.txt"
    print "Output file :" + output_file_name
    getProgramPoint(args.csv,output_file_name)

