import numpy as np
import FC_Score_Algo2,time
import random
def readData1(fileName):
    with open(fileName, "r") as fp:
        # with open("35X5000.txt","r") as fp:
        data = []
        d = {}
        index = 0
        # sindex=0
        for line in fp.readlines():
            # print tuple(line.split())
            # line=line.replace("\"","")
            line = [int(i) for i in line.split()]
            data.append(line)
            d[index] = index
            # sindex+=1
            index += 1


            # data=data[34755:43011]
    #print len(data[0])
    data = np.array(data)
    y = []
    for j in range(len(data[0])):
        if sum(data[:, j]) >= 1:
            y.append(1)
        else:
            y.append(0)

    #print y, sum(y)
    return y, data, d

dataFile = "Data/closure4/closure_data.txt" #r
fun_idF="Data/closure4/fun_id.txt" #r
fun_invF="Data/closure4/funID_invariant.txt"
fun_funF="Data/closure4/fun_fun.txt"



fun_idF="Data/closure4/fun_id.txt" #r
fun_invF="Data/closure4/funID_invariant.txt"
fun_funF="Data/closure4/fun_fun.txt"

paths=["Data/closure2new/closure-","Data/closure2oldest/closure-","Data/closure2r/closure-"]


fun_idF="Data/time12/fun_id.txt" #r
fun_invF="Data/time12/funID_invariant.txt"
fun_funF="Data/time12/fun_fun.txt"
# TimeNewToOld /TimeOlderToNew /TimeRandom


paths=["Data/TimeNewToOld/time11-","Data/TimeOlderToNew/time12-","Data/TimeRandom/time12-"]





paths=["Data/math/codecoverage/math-"]

paths=["Data/codecoverage/Closure15-"]

paths=["Data/time_codecoverage/time15-"]

paths=["Data/collection_codecoverage/collection-"]
dd=0
rf=open(paths[dd]+"_Algo-2b_Result"+paths[dd].split("/")[2]+"_Feb12.txt","a+")  

for k in [10,20,30,40,50,60,70,80,90] :#[10,20,30,40,50,60,70,80,90]
    fun_idF="Data/math/codecoverage/fun_id-"+str(k)+".txt" #r
    fun_invF="Data/math/codecoverage/funID_invariant-"+str(k)+".txt"
    fun_funF="Data/math/codecoverage/math"+str(k)+".txt"
    
    fun_idF="Data/codecoverage/fun_id-Closure-"+str(k)+".txt" #r
    fun_invF="Data/codecoverage/funID_invariant-Closure-"+str(k)+".txt"
    fun_funF="Data/codecoverage/Closure"+str(k)+".txt"
    
    fun_idF="Data/time_codecoverage/fun_id-time-"+str(k)+".txt" #r
    fun_invF="Data/time_codecoverage/funID_invariant-time-"+str(k)+".txt"
    fun_funF="Data/time_codecoverage/time"+str(k)+".txt"
    
    fun_idF="Data/collection_codecoverage/fun_id-collection-"+str(k)+".txt" #r
    fun_invF="Data/collection_codecoverage/funID_invariant-collection-"+str(k)+".txt"
    fun_funF="Data/collection_codecoverage/collection"+str(k)+".txt"
    
    dataFile = paths[dd]+str(k)+".txt" #r
    for runTime in range(1):
        rf.write(">>>>>>>>>>>>>>>>>>----- "+str(runTime)+" -----<<<<<<<<<<<<<<<<<<<\n")
        print ">>>>>>>>>>>>>>>>>>----- ",runTime," -----<<<<<<<<<<<<<<<<<<<" 

        fun_id_dict = {}
        with open(fun_idF, "r") as c_idF:
            for line in c_idF.readlines():
                e = line.replace("\n", "").replace("\r", "").split(" ")
        
                fun_id_dict[e[0]] = int(e[1])
        N = len(fun_id_dict)
        #print "N=",N,fun_id_dict.values()[:20]
        
        
        
        """ Get each function invariant list """
        # print class_id_dict,len(class_id_dict)
        fun_invar_List = [[] for i in range(N)]
        # print len(class_invar_List)
        
        with open(fun_invF, "r") as c_i_F:
            for line in c_i_F.readlines():
                terms=line.split()
                #print terms[0],terms[1]
                fun_invar_List[int(terms[0])].append(terms[1])
        #print "fun_invarian",fun_invar_List[100]
        """ Get function function relation, and generate adjcent list"""
        edges = []
        with open(fun_funF, "r") as c_c_F:
            for line in c_c_F.readlines():
                #print "L",line
                e = line.replace("\n", "").replace(":", ".").split()
                #print "e",e
                if e[0] != e[1] and fun_id_dict.has_key(e[0]) and fun_id_dict.has_key(e[1]):
                    #print "edges:",e
                    edges.append((fun_id_dict[e[0]], fun_id_dict[e[1]]))
        #print len(edges)
        
        
        """ Read Invariant Raw data"""
        y,V,d=FC_Score_Algo2.readData1(dataFile)
        
        adjList = np.zeros((N, N))
        for e in edges:
            adjList[e[0]][e[1]] = 1
            adjList[e[1]][e[0]] = 1
            # pprint.pprint(adjList)
        # pprint.pprint(np.sum(adjList,axis=1))
        B_I = int(0.001*len(d))
        B_C = int(0.1*len(fun_id_dict))
        
        S_star = []
        C_star = []
        
        sorted_degree = []
        for i, deg in enumerate(adjList):
            #print sum(deg)
            sorted_degree.append((i, sum(deg)))
        #print sorted_degree
        sorted_degree = sorted(sorted_degree, key=lambda tup: tup[1], reverse=True)
        #print "Sorted Degree",sorted_degree
        for degree in sorted_degree:
            if len(C_star) <= B_C and degree[0] not in C_star:
                C_star.append(degree[0])
                iMax_deg = 0
                temp_index = 0
                for c in fun_invar_List[degree[0]]:
                    c=int(c)
                    #print fun_invar_List[degree[0]]
                    #print "####",c,V[c],sum(V[c]),iMax_deg
                    if sum(V[c]) > iMax_deg:
                        iMax_deg = sum(V[c])
                        temp_index = c
                #print temp_index, sum(V[temp_index]), V[temp_index]
                if sum(V[temp_index]) != 0:
                    S_star.append(temp_index)
                else:
                    S_star.append(random.choice(fun_invar_List[degree[0]]))
        S_star=list(set(S_star))
        print dataFile,"==============="
        print len(S_star)," Invariant:", S_star
        rf.write(dataFile+"============================\n")
        rf.write(",".join(map(str,S_star))+"\n")
rf.close()
#print len(C_star), "functions:",C_star
#print time.time()-t0,' sec....'



# fun_call=open("Data/batchAgent/fun_invariant.txt","w")
#
# with open("Data/batchAgent/fi.txt","r") as ff:
#     for i,line in enumerate(ff.readlines()):
#         if line.find("(")>0:
#             functionName=line.split("(")[0]
#             fun_call.write(functionName+" "+str(i)+"\n")
#             print i,functionName
#         else:
#             continue

# fun_fun=open("Data/batchAgent/fun_fun.txt","w")
#
# with open("Data/batchAgent/callgraph_unique.txt","r") as cF:
#     for line in cF.readlines():
#         if line.find("<")>0:
#             continue
#
#         terms=line.split()
#         if len(terms) < 4:
#             continue
#         if len(terms)>4:
#             print "#\n#\n#\n#\n#\n#\n#\n#\n#\n#\n#\n#\n#\n#\n#\n#\n#\n#\n#\n#\n#\n#\n#\n"
#         print len(terms),"-----------------------------------------------------------\n",line
#         fun_fun.write(terms[0]+"."+terms[1]+" "+terms[2]+"."+terms[3]+"\n")
#         print terms[0]+"."+terms[1],terms[2]+"."+terms[3]

