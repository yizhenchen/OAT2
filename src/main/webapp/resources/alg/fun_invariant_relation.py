import csv
import sys
import time
import argparse
import random
csv.field_size_limit(922337203)
import os
dir_path = os.path.dirname(os.path.realpath(__file__))

def getFun_Invariant(raw_data,fun_in):
    f = open(fun_in, 'w' )
    with open(raw_data,"r") as files:
        reader = csv.DictReader(files)
        writer = csv.DictWriter(f, fieldnames=reader.fieldnames, lineterminator='\n')
        writer.writeheader()

        for row in reader:
            str = row["Program Points"]
            if(":::ENTER" in str or ":::EXIT"  in str ):
                if ("(" in str):
                    row["Program Points"] = str[0:str.find("(")]
                    writer.writerow(row)
                else:
                    print row["Program Points"]
            else:
                writer.writerow(row)
    print "Done"


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Get Function_invariant file")
    parser.add_argument('csv')
    args = parser.parse_args()
    print args.csv
    output_file_name = dir_path+"/Data/"+str(time.strftime("%d%m%Y%I%M%S"))+str(random.randint(1, 10))+"_fun_invariant.txt"
    print "Output file :" + output_file_name
    getFun_Invariant(args.csv,output_file_name)
