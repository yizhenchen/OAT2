<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<!doctype html>
<!--
	Template:	 Unika - Responsive One Page HTML5 Template
	Author:		 imransdesign.com
	URL:		 http://imransdesign.com/
    Designed By: https://www.behance.net/poljakova
	Version:	1.0	
-->
<html lang="en-US">
	<head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Online Software Anomaly Detection</title>
		<meta name="description" content="Unika - Responsive One Page HTML5 Template">
		<meta name="keywords" content="HTML5, Bootsrtrap, One Page, Responsive, Template, Portfolio" />
		<meta name="author" content="imransdesign.com">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- Google Fonts  -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,500' rel='stylesheet' type='text/css'> <!-- Body font -->
		<link href='http://fonts.googleapis.com/css?family=Lato:300,400' rel='stylesheet' type='text/css'> <!-- Navbar font -->

		<!-- Libs and Plugins CSS -->
		<link rel="stylesheet" href="resource/inc/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="resource/inc/animations/css/animate.min.css">
		<link rel="stylesheet" href="resource/inc/font-awesome/css/font-awesome.min.css"> <!-- Font Icons -->
		<link rel="stylesheet" href="resource/inc/owl-carousel/css/owl.carousel.css">
		<link rel="stylesheet" href="resource/inc/owl-carousel/css/owl.theme.css">

		<!-- Theme CSS -->
        <link rel="stylesheet" href="resource/css/reset.css">
		<link rel="stylesheet" href="resource/css/style.css">
		<link rel="stylesheet" href="resource/css/mobile.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="resource/css/skin/cool-gray.css">
        <!-- <link rel="stylesheet" href="css/skin/ice-blue.css"> -->
        <!-- <link rel="stylesheet" href="css/skin/summer-orange.css"> -->
        <!-- <link rel="stylesheet" href="css/skin/fresh-lime.css"> -->
        <!-- <link rel="stylesheet" href="css/skin/night-purple.css"> -->

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>

    <body data-spy="scroll" data-target="#main-navbar">
        <div class="page-loader"></div>  <!-- Display loading image while page loads -->
    	<div class="body">
        
            <!--========== BEGIN HEADER ==========-->
            <header id="header" class="header-main">

                <!-- Begin Navbar -->
                <nav id="main-navbar" class="navbar navbar-default navbar-fixed-top" role="navigation"> <!-- Classes: navbar-default, navbar-inverse, navbar-fixed-top, navbar-fixed-bottom, navbar-transparent. Note: If you use non-transparent navbar, set "height: 98px;" to #header -->

                  <div class="container">

                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      
                      <a class="navbar-brand page-scroll" href="index.html">Online Software Anomaly Detection</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                           <!--  <li><a class="page-scroll" href="body">Home</a></li>
                            <li><a class="page-scroll" href="#about-section">About</a></li>
                            <li><a class="page-scroll" href="#services-section">Services</a></li>
                            <li><a class="page-scroll" href="#portfolio-section">Works</a></li>
                            <li><a class="page-scroll" href="#team-section">Team</a></li>                            
                            <li><a class="page-scroll" href="#prices-section">Prices</a></li> -->
                            <li><a class="page-scroll" href="#contact-section">Help Info</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container -->
                </nav>
                <!-- End Navbar -->

            </header>
            <!-- ========= END HEADER =========-->
            
            
            
            
        
                
                
                
                
            <!-- Begin about section -->
			<section id="upload-section" class="page bg-style1 parallax" style="padding-top: 125px;">
                <!-- Begin page header-->
                <div class="page-header-wrapper">
                    <div class="container">
                        <div class="page-header text-center " >
                            <h2>Step By Step</h2>
                            <div class="devider"></div>
                           
                        </div>
                    </div>
                </div>
                <!-- End page header-->

                <!-- Begin rotate box-1 -->
                <div class="rotate-box-1-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                               
                                    <div class="rotate-box-info">
                                        <h4>1. Get Invariant Metrix</h4>
                                        <p>Invariant Metrix is a csv file , contains all the invariants. To obtain invariants metrix, you have to run your program has faults and program which known faults be fixed, with daikon and get invariants.
                                        Compare the program has faults' invariants with the program which known faults be fixed's invariants, get Invariant Metrix.      
                                         </p>
                                         <br><br>
                                    </div>
                               
                            </div>
                            </div>
                         <div class="row">
                            <div class="col-md-12 col-sm-12">
                                    <div class="rotate-box-info">
                                      <h4>2.Get Class RelationShip</h4>
                                        <p>To obtain the Class Relationship you could use ObjectAid,a tool for the Eclipse, to generate Class Daigrm, which is a ucls file.
                                        The objectaid offical web site is : http://www.objectaid.com/ </p>
                                        <br><br>
                                    </div>
                                </div>
                            </div>
						  <div class="row">
                            <div class="col-md-12 col-sm-12">
                            
                                    <div class="rotate-box-info">
                                        <h4>3. Get Call Graph </h4>
                                        <p>Call Graph is a control flow graph, which represents calling relationships between functions.We use Java-callgraph tool, avaliable on github. You could find it by the link https://github.com/gousiosg/java-callgraph </p><br><br>
                                    </div>
                 
                               </div>
                            
                           <div class="row">
                            <div class="col-md-12 col-sm-12">
                               
                                    <div class="rotate-box-info">
                                        <h4>4. Upload Files</h4>
                                        <p>Once you get these 3 file. Upload files here.</p>
                                     <br><br>
								</div>
                            </div>
                            
                        </div> <!-- /.row -->
                    </div> <!-- /.container -->
                </div>
                <!-- End rotate box-1 -->
                
                <div class="extra-space-l"></div>
        
        <div>

                          <form id="uploadfile" method="POST" action="uploadMultipleFile" enctype="multipart/form-data">
          <h4>Choose Invariant Metrix</h4>            
       <input type="file" id="invariantMetrixFile" name="invariantMetrixFile" class="btn btn-default btn-lg" ><br /> 

        <h4>Choose  Call Graph </h4>
        <input type="file" id="callgraphFile" name="callgraphFile" class="btn btn-default btn-lg"><br /> 
        <h4>Choose  Class Graph </h4>
         <input type="file" id="classGraphFile" name="classGraphFile" class="btn btn-default btn-lg"><br /> 
     
        <input type="submit" value="Upload"  class="btn btn-default btn-lg"> 
    </form>

                </div>
              
          </section>
          <!-- End about section -->

        
              
               <section id="cta-section">
          	<div class="cta">
            	<div class="container">
                	<div class="row">
                    
                    	<div class="col-md-9">
                        	<div id="uploaded_file">
                                <h1>None file be uploaded</h1>
                             </div>
                            
                        </div>
                        
                        <div class="col-md-3">
                        	<div class="cta-btn" >
                        <a class="btn btn-default btn-lg"  id="btn_analysis" target="_blank" role="button">Start Analysis</a>
                        	</div>
                        </div>
                        
                    </div> <!-- /.row -->
                </div> <!-- /.container -->
            </div>
          </section>
             
 <div id="messages"></div>
           
                
            <!-- Begin contact section -->
			<section id="contact-section" class="page text-white parallax" data-stellar-background-ratio="0.5" style="background-image: url(resource/img/map-bg.jpg);">
            <div class="cover"></div>
            
                 <!-- Begin page header-->
                <div class="page-header-wrapper">
                    <div class="container">
                        <div class="page-header text-center " >
                            <h2>Contacts</h2>
                            <div class="devider"></div>
                            <p class="subtitle">All to contact us</p>
                        </div>
                    </div>
                </div>
                <!-- End page header-->
                
                <div class="contact " >
                    <div class="container">
                    	<div class="row">
                        
                            <div class="col-sm-6">
                                <div class="contact-info">
                                    <h4>Our Address</h4>
                                    <ul class="contact-address">
			                            <li><i class="fa fa-map-marker fa-lg"></i>&nbsp; Computer Science Department,<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1215 Western Ave. UAB 403,<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Albany, NY 12222</li>
			                            <li><i class="fa fa-phone"></i>&nbsp; (518) 442-4283 </li>
			                            <li><i class="fa fa-print"></i>&nbsp; (518) 442-5638</li>
			                            <li><i class="fa fa-envelope"></i>  (ychen33, mying2, dliu, aalimu)@albany.edu</li>
			                            <!-- <li><i class="fa fa-skype"></i> Unika-Design</li> -->
			                        </ul>
                                </div>
                            </div>
                        
                        	<div class="col-sm-6">
                                <div class="contact-form">
                                	<h4>Write to us</h4>
                                    <form role="form">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-lg" placeholder="Your Name" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control input-lg" placeholder="E-mail" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control input-lg" placeholder="Subject" required>
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control input-lg" rows="5" placeholder="Message" required></textarea>
                                        </div>
                                        <button type="submit" class="btn">Send</button>
                                    </form>
                                </div>	
                            </div>
                                                                                
                        </div> <!-- /.row -->
                    </div> <!-- /.container -->
                </div>
            </section>
            <!-- End contact section -->
                
                

                
            <!-- Begin footer -->
            <footer class="text-off-white">
            
              
                  
                
                <div class="footer">
                    <div class="container text-center " >
                        <p class="copyright">Copyright &copy; 2015 - Designed By <a href="https://www.behance.net/poljakova" class="theme-author">Veronika Poljakova</a> &amp; Developed by <a href="http://www.imransdesign.com/" class="theme-author">Imransdesign</a></p>
                    </div>
                </div>

            </footer>
            <!-- End footer -->

            <a href="#" class="scrolltotop"><i class="fa fa-arrow-up"></i></a> <!-- Scroll to top button -->
                                              
        </div><!-- body ends -->
        
        
        
        
        <!-- Plugins JS -->
		<script src="resource/inc/jquery/jquery-1.11.1.min.js"></script>
		<script src="resource/inc/bootstrap/js/bootstrap.min.js"></script>
		<script src="resource/inc/owl-carousel/js/owl.carousel.min.js"></script>
		<script src="resource/inc/stellar/js/jquery.stellar.min.js"></script>
		<script src="resource/inc/animations/js/wow.min.js"></script>
        <script src="resource/inc/waypoints.min.js"></script>
		<script src="resource/inc/isotope.pkgd.min.js"></script>
		<script src="resource/inc/classie.js"></script>
		<script src="resource/inc/jquery.easing.min.js"></script>
		<script src="resource/inc/jquery.counterup.min.js"></script>
		<script src="resource/inc/smoothscroll.js"></script>

		<!-- Theme JS -->
		<script src="resource/js/theme.js"></script>
		<script src="resource/js/notify.js"></script>
        <script type="text/javascript">

        

        var webSocket;
        var messages = document.getElementById("messages");
        function openSocket(){
               // Ensures only one connection is open at a time
               if(webSocket !== undefined && webSocket.readyState !== WebSocket.CLOSED){
                  writeResponse("WebSocket is already opened.");
                   return;
               }
               // Create a new instance of the websocket
               webSocket = new WebSocket("ws://localhost:8080/InvariantAnalysis/websocket/echo");
                
               /**
                * Binds functions to the listeners for the websocket.
                */
               webSocket.onopen = function(event){
                   // For reasons I can't determine, onopen gets called twice
                   // and the first time event.data is undefined.
                   // Leave a comment if you know the answer.
                   if(event.data === undefined)
                       return;

                   writeResponse(event.data);
               };

               webSocket.onmessage = function(event){
                   writeResponse(event.data);
               };

               webSocket.onclose = function(event){
                   writeResponse("Connection closed");
               };
           }

           /**
            * Sends the value of the text input to the server
            */
           function send(){
               var text = document.getElementById("messageinput").value;
               webSocket.send(text);
           }
          
           function closeSocket(){
               webSocket.close();
           }

           function writeResponse(text){
               messages.innerHTML += "<br/>" + text;
           }
          
       $("#btn_analysis").click(function(){
    	   openSocket()
       })

       $( '#uploadfile' ).submit( function( e ) {
   var flag =true
   if($("#invariantMetrixFile").val()==""){
       $.notify("Warning: Invariants Metrix Files not be provided .", "warn");
       flag =false
   }
   if($("#callgraphFile").val()==""){
        $.notify("Warning: Call graph Files not be provided .", "warn");
        flag =false
       
   }
   if($("#classGraphFile").val()==""){
       $.notify("Warning: Class graph Files not be provided .", "warn");
       flag =false
   }
   if(!flag){
   // $.ajax( {
   //   url: 'uploadFile',
   //   type: 'POST',
   //   data: new FormData( this ),
   //   processData: false,
   //   contentType: false
   // }).done(function(msg){
   //  console.log(msg);
       
   // });


       var msg ="@uploaded file @ 201702260959324invariantMetrix.txt@uploaded file @ 2017022609593210callgraphFile.txt@uploaded file @ 201702260959329classGraphFile.txt"
      // msg = "asfas  asfasf asfasdf"
       if(msg.indexOf("@")==-1){
            $.notify("Warning: Class graph Files not be provided .", "error");
       }else{
           $.notify("Files uploaded ! ", "success");
          var msgArr = msg.split("@");
               msgArr[2]
               msgArr[4]
               msgArr[6] 
                $("#uploaded_file").empty()
               var html ="<div class='row' >"+
                       "<div class='col-sm-4'>"+
                       "    <img src='resource/img/file.jpg' style='width: 90%;'> <br><p><b>file name :"+
                        msgArr[2]+
                       "</b></p></div>"+
                       "<div class='col-sm-4'>"+
                       "    <img src='resource/img/file.jpg' style='width: 90%;'><br><p><b>file name :"+
                        msgArr[4]+
                       "</div>"+
                       "<div class='col-sm-4'>"+
                       "    <img src='resource/img/file.jpg' style='width: 90%;'><br><p><b>file name :"+
                        msgArr[6]+
                       "</div>"+
                       "</div>"+
                       "<div id='hiddenInput' style='display:none'>"+
                       msgArr[2]+"@"+msgArr[2]+"@"+msgArr[4]+"</div>";


               $("#uploaded_file").append(html)
       }
       
   }
   e.preventDefault();
 } );

       

        </script>
    </body> 
        
            
</html>
