# -*- coding: utf-8 -*-
"""
Created on Sat Feb 18 20:08:35 2017

@author: Adil
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Nov 18 11:36:14 2015

@author: Adil
"""


import numpy as np
import time, random ,sys
import argparse
import csv
from operator import itemgetter
csv.field_size_limit(922337203)
import os
dir_path = os.path.dirname(os.path.realpath(__file__))


""" Cost fucnction c(S)"""
def C(S):
    return len(S)

""" Objective score function"""
def F(S,V,y):
    f=[]
    FS=0.0
    FS_0=0.0
    #print 'y_len',len(y)
    for i in range(len(y)):
        temp=0.0
        for j in S:            
            temp+=V[j][i]
        f.append(temp)
    
    
    T=1.0/sum(y)
    
    for j in range(len(y)):
        if f[j]>=1:
            f[j]=1
        temp=y[j]*f[j]
        #if temp!=0:
            #print y[j],f[j]
        FS_0+=y[j]*f[j]
    
    #print sum(y)
    #print FS_0,S
    #print '---------------------------'
    #for j in range(len(y)):
    #    print f[j],y[j]
    
    FS=T*FS_0
    
    return FS

""" Read Invariant-Test Case Data MAtrix 
        Input the have test case failure invariant information
        
        Output: 1) test case vector : y
                2) data matrix nXm , n: number of the invariants with test case failure
                                     m: test case number
                3) invariant index mapping dictionary. invariant index into line number in cvs file
"""
def readData(fileName):
    with open(fileName,"r") as fp:    
        data=[]
        d={}
        realIndex=0
        sindex=0
        for line in fp.readlines():
            #print tuple(line.split())
            #line=line.replace("\"","")
            line=[int(i) for i in line.strip().split()]
            if sum(line)>0:
                #print sindex,realIndex
                data.append(line)
                d[sindex]=realIndex
                sindex+=1            
            realIndex+=1    
    
    data=np.array(data)
    y=[]
    for j in range(len(data[0])):
        if sum(data[:,j])>=1:
            y.append(1)
        else:
            y.append(0)

    return y,data,d   
    
def S_0(V,y,lambd1):
    S0=[]
    GScore_temp=-1000.0    
    inti_index=0
    t_l=[]
    gs_t=[]
    for i in range(len(V)):
        t_l[:]=[]        
        t_l.append(i)        
        tGS=GS(t_l,V,y,lambd1)
        print t_l ,tGS       
        gs_t.append(tGS)
        if tGS>GScore_temp:                        
            GScore_temp=tGS
            t_l.pop()
            inti_index=i
    #print '>>#########',lambd1,'#########'
    S0.append(inti_index)
    #print 's0',tindex
    #print S0,GScore_temp
    return S0,GScore_temp    
    
def GS(S,V,y,lambd1):
    #print 'S',S
    #print 'FS',F(S,V,y)
    #print 'C(S)',lambd*C(S)
    G=F(S,V,y)-lambd1*C(S)
    return round(G,4)
    
def cros_mat(S_c,S,cov):
    cros_Mat=np.zeros((len(S_c),len(S)))
    for q,i in enumerate(S_c):
        for k,j in enumerate(S):
            cros_Mat[q][k]=cov[i][j]
    return cros_Mat
    

    
    
def Alg1(dataFile,outputFile):

    output = open(outputFile, "a+")
    ## read data
    y,V,d=readData(dataFile)      
    lambd1=[i/100.0 for i in range(1,101,4)]
 
    SL=[]
    t0=time.time()
    ##budget constrain
    BB=int(1*len(d))

    for B in range(BB,BB+1):
        for l1 in range(len(lambd1)):            
            GScore_temp=0.0   #           
            S=[]
            S_back=[]
            S_t=[]                           
            for e in range(len(V)):                    
                if e not in S:                         
                    S_t.append(e)           
                    """ SCORE VALUE WITH ADDED ELEMENT e """
                    GScore_temp_e=GS(S_t,V,y,lambd1[l1]) 
                    """ SCORE VALUE WITHout ELEMENT e """
                    GScore_temp=GS(S,V,y,lambd1[l1])     
                    """if the score funciton is increased we keed the element, otherwise remove it."""
                    if GScore_temp_e>=GScore_temp:                                               
                        S.append(e)
                    else:
                        S_t.remove(e)
                       
                """check the budget, if exceed  then return  result"""     
                if len(S)>B:
                    break
            
            """mapping the invariant index into line number"""
            sS=[]            
            for iS in S:
                sS.append(d[iS])
            SL.append((B,lambd1[l1],GS(S,V,y,lambd1[l1]),round(F(S,V,y),2),len(sorted(S)),sorted(sS)))
    """Sort the results base on the score"""
    SL=sorted(SL, key=itemgetter(2),reverse=True)      
    """Only return the top 1st result"""
    for ss in SL[:1]:
        output.write(",".join(map(str,ss[5]))+"\n")        
    print "Running time :" + str(time.time()-t0)+" sec..."
    print "done"
    output.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Step2: Run with Alg1")
    parser.add_argument('csv')
    parser.add_argument('path')
    args = parser.parse_args()
    print args.csv
    output_file_name = dir_path+"/Data/"+ str(time.strftime("%d%m%Y%I%M%S")) + str(random.randint(1, 10)) + "_alg1_result.txt"
    print "Output file :" + output_file_name
    Alg1(args.csv, output_file_name)
