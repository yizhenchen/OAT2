# -*- coding: utf-8 -*-
"""
Created on Wed Nov 18 11:36:14 2015

@author: Adil
"""


import numpy as np
import mutual_info,time
from operator import itemgetter


def C(S):
    return len(S)

def F(S,V,y):
    f=[]
    FS=0.0
    FS_0=0.0
    #print 'y_len',len(y)
    for i in range(len(y)):
        temp=0.0
        for j in S:            
            temp+=V[j][i]
        f.append(temp)
    
    
    T=1.0/sum(y)
    
    for j in range(len(y)):
        if f[j]>=1:
            f[j]=1
        temp=y[j]*f[j]
        #if temp!=0:
            #print y[j],f[j]
        FS_0+=y[j]*f[j]
    
    #print sum(y)
    #print FS_0,S
    #print '---------------------------'
    #for j in range(len(y)):
    #    print f[j],y[j]
    
    FS=T*FS_0
    
    return FS
def readData1():
    with open("Data/time3_correct_withindex_data.txt","r") as fp:
    #with open("35X5000.txt","r") as fp:
        data=[]
        d={}
        index=0
        sindex=0
        for line in fp.readlines():
            #print tuple(line.split())
            #line=line.replace("\"","")
            line=[int(i) for i in line.split()]
            if sum(line)>0:
                data.append(line)
                d[sindex]=index
                sindex+=1
                
            
            index+=1    
                
            
    #data=data[34755:43011]        
    print len(data[0])
    data=np.array(data)
    y=[]
    for j in range(len(data[0])):
        if sum(data[:,j])>=1:
            y.append(1)
        else:
            y.append(0)
        
    print y,sum(y)
    return y,data,d    

   
def S_0(S,V,y,lambd1):
    S0=[]
    GS_temp=-1000.0    
    tindex=0
    t_l=[]
    gs_t=[]
    for i in range(len(V)):
        t_l=S        
        t_l.append(i)        
        tGS=GS(t_l,V,y,lambd1)
        #GS(t_l,V,y,lambd)
        gs_t.append(tGS)
        if tGS>GS_temp:                        
            GS_temp=tGS
            t_l.pop()
            tindex=i
    print '>>#########',lambd1,'#########'
    S0.append(tindex)
    #print 's0',tindex
    #print S0,GS_temp
    return S0    
    
def GS(S,V,y,lambd1):
    #print 'S',S
    #print 'FS',F(S,V,y)
    #print 'C(S)',lambd*C(S)
    G=F(S,V,y)-lambd1*C(S)
    return round(G,4)
    
def cros_mat(S_c,S,cov):
    cros_Mat=np.zeros((len(S_c),len(S)))
    for q,i in enumerate(S_c):
        for k,j in enumerate(S):
            cros_Mat[q][k]=cov[i][j]
    return cros_Mat
    

    
    
def main():
    
    rff=open("Data/time3_correct_withindex_result.txt","w")
    
    y,V,d=readData1()
    #GV=[i for i in range(34)]
    #lambd1=[i/10.0 for i in range(1,11)]
    print len(V)
    lambd1=[0.1]
    #lambd2=[1]
    #S=[i for i in range(10,30)]
    #print 'M1  -  M2 = M'
    #for k in range(14):
    #    print M(S,GV,cov,k)
    #K=5
    SL=[]
    for B in range(5,16):
        for K in range(1,2):
            for l1 in range(len(lambd1)):
                for l2 in range(1,2):
                    GS_temp=0.0
                    gs_t=[]
                    tindex=0
                    t_l=[]
                    S=[]    
                    print l1,l2
                    S=S_0(S,V,y,lambd1[l1])
                    print '=====',S,len(V)
                    #print 'S:',S
                    for v in range(len(V)):
                        print'_>',S, v,len(V)
                        GS_t=GS(S,V,y,lambd1[l1])        
                        #print 'GE_temp',GS_t
                        t_l=S
                        for u in range(len(V)):
                            if u not in S:
                                #print 'u=',u
                                t_l.append(u)
                                #print t_l
                                GS_temp=GS(t_l,V,y,lambd1[l1])
                                if GS_temp>GS_t:
                                    GS_t=GS_temp
                                    tindex=u
                                    t_l.remove(u)
                                else:
                                    t_l.remove(u)
                        #print 'new',tindex
                        if len(S)<=B and S[len(S)-1]!=tindex:
                            S.append(tindex)
                        else:
                            break
                    #SL.append((GS(S,V,y,lambd[lm]),S))
                    #print 'B=',B,'>>>',F(S,V,y)
                    #time.sleep(5)
                    sS=[]
                    
                    for iS in S:
                        sS.append(d[iS])
                    SL.append((B,lambd1[l1],GS(S,V,y,lambd1[l1]),round(F(S,V,y),2),len(sorted(S)),sorted(sS)))
            
    print 'B lmbda1 lambda2 G(S) F(S) C(S) M(S)'
    SL=sorted(SL, key=itemgetter(3),reverse=True)
    
    rff.write("B lmbda1 lambda2 G(S) F(S) C(S) M(S)]\n")
    
    for ss in SL:
        rff.write(str(ss).replace("(","").replace(")","").replace(","," ")+"\n")        
    print 'done!!!!'#   print ss
    
    rff.close()
    
    
    #print GS([34, 18, 4, 19, 27, 24, 22, 13, 5, 31, 21],V,y,0.6)
if __name__ == '__main__':
    main()
        
            