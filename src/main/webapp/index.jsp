<!doctype html>



<html lang="en-US">
	<head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Online Software Anomaly Detection</title>
		<meta name="description" content="Online Software Anomaly Detection tools">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- Google Fonts  -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,500' rel='stylesheet' type='text/css'> <!-- Body font -->
		<link href='http://fonts.googleapis.com/css?family=Lato:300,400' rel='stylesheet' type='text/css'> <!-- Navbar font -->

		<!-- Libs and Plugins CSS -->
		<link rel="stylesheet" href="resource/inc/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="resource/inc/animations/css/animate.min.css">
		<link rel="stylesheet" href="resource/inc/font-awesome/css/font-awesome.min.css"> <!-- Font Icons -->
		<link rel="stylesheet" href="resource/inc/owl-carousel/css/owl.carousel.css">
		<link rel="stylesheet" href="resource/inc/owl-carousel/css/owl.theme.css">

		<!-- Theme CSS -->
        <link rel="stylesheet" href="resource/css/reset.css">
		<link rel="stylesheet" href="resource/css/style.css">
		<link rel="stylesheet" href="resource/css/mobile.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="resource/css/skin/cool-gray.css">
        <!-- <link rel="stylesheet" href="css/skin/ice-blue.css"> -->
        <!-- <link rel="stylesheet" href="css/skin/summer-orange.css"> -->
        <!-- <link rel="stylesheet" href="css/skin/fresh-lime.css"> -->
        <!-- <link rel="stylesheet" href="css/skin/night-purple.css"> -->

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>

    <body data-spy="scroll" data-target="#main-navbar">
        <div class="page-loader"></div>  <!-- Display loading image while page loads -->
    	<div class="body">
        
            <!--========== BEGIN HEADER ==========-->
            <header id="header" class="header-main">

                <!-- Begin Navbar -->
                <nav id="main-navbar" class="navbar navbar-default navbar-fixed-top" role="navigation"> <!-- Classes: navbar-default, navbar-inverse, navbar-fixed-top, navbar-fixed-bottom, navbar-transparent. Note: If you use non-transparent navbar, set "height: 98px;" to #header -->

                  <div class="container">

                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      
                      <a class="navbar-brand page-scroll" href="/InvariantAnalysis">Online Software Anomaly Detection</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                           <!--  <li><a class="page-scroll" href="body">Home</a></li>
                            <li><a class="page-scroll" href="#about-section">About</a></li>
                            <li><a class="page-scroll" href="#services-section">Services</a></li>
                            <li><a class="page-scroll" href="#portfolio-section">Works</a></li>
                            <li><a class="page-scroll" href="#team-section">Team</a></li>                            
                            <li><a class="page-scroll" href="#prices-section">Prices</a></li> -->
                            <li><a class="page-scroll" href="#contact-section">Help Info</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container -->
                </nav>
                <!-- End Navbar -->

            </header>
            <!-- ========= END HEADER =========-->
            
            
            
            
        	<!-- Begin text carousel intro section -->
			<section id="text-carousel-intro-section" class="parallax"  style="background-image: url(resource/bg.jpg);">
				
				<div class="container">
					<div class="caption text-left text-white">

						<div>
						<div class="col-md-6 col-sm-12">
						</div>
							<div class="col-md-6 col-sm-12">
								<h2> </h2>
								<p style="background: #eebb55; padding: 20px;"><b>Automatic online software failure detection</b> is crucial for ensuring the quality of the deployed software, but it is extremely challenging to perform in practice. Existing approaches attempt to tackle this problem by identifying certain properties observed from the software executions during the training phase and using them to monitor software online anomalous behaviors. Nevertheless, the large execution overhead required for monitoring these properties limits the applicability of these approaches at runtime. We present a model that applies the sensor placement algorithms to select a close to optimal set of anomaly-revealing invariants which can detect online anomaly with minimal execution overhead.</p><Br>
<p style="background: #eebb55; padding: 20px;">
This website implements such a tool that can give the optimal set of anomaly-revealing invariants. </p>
                                <div class="extra-space-l"></div>
								<a href="invariantselection.jsp" class="btn btn-blank" target="_blank" role="button" style="background: #eebb55;">Try it on</a>
							</div>
						
						</div>

					</div> <!-- /.caption -->
				</div> <!-- /.container -->

			</section>
			<!-- End text carousel intro section -->
                
                
                
                
            <!-- Begin about section -->
			<section id="about-section" class="page bg-style1">
                <!-- Begin page header-->
                <div class="page-header-wrapper">
                    <div class="container">
                        <div class="page-header text-center " >
                            <h2>About Tools</h2>
                            <div class="devider"></div>
                           
                        </div>
                    </div>
                </div>
                <!-- End page header-->

                <!-- Begin rotate box-1 -->
                <div class="rotate-box-1-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <a href="#" class="rotate-box-1 square-icon ">
                                    <span class="rotate-box-icon"><i class="fa fa-users"></i></span>
                                    <div class="rotate-box-info">
                                        <h4>What it is?</h4>
                                        <p>
Online Software Anomaly Detection tool helps to determine  the critical invariants which help detect software anomalous behavior, which would then require a need to set monitors to check the status of these invariants. It is possible then to detect software anomalies by checking for abnormal values of the monitored invariants.
</p>  </div>
                                </a>
                            </div>

                            <div class="col-md-12 col-sm-12">
                                <a href="#" class="rotate-box-1 square-icon " >
                                    <span class="rotate-box-icon"><i class="fa fa-diamond"></i></span>
                                    <div class="rotate-box-info">
                                        <h4>What files we need?</h4>
                                        <p>First, the Daikon invariant detector is used to generate the program invariants.  The Daikon tool can be found in the following link: http://plse.cs.washington.edu/daikon/. Additionally 3 files are required before applying the necessary algorithms. Second, a historical fault report is needed to seed the faults to the program. In order to obtain the invalid invariants set, the program must be re-run with faults to generate a set of invariants that will be compared with that of the non-faulty program. The class diagram and function call graph are used to describe the structure of the system by showing the system’s classes and their attributes, operations (or methods), and relationships among objects.</p>
                                    </div>
                                </a>
                            </div>

                            <div class="col-md-12 col-sm-12">
                                <a href="#" class="rotate-box-1 square-icon ">
                                    <span class="rotate-box-icon"><i class="fa fa-heart"></i></span>
                                    <div class="rotate-box-info">
                                        <h4>What is the output?</h4>
                                        <p> It will determine the optimal set of anomaly-revealing invariants base on a sensor placement algorithm.  </p>
                                    </div>
                                </a>
                            </div>
                            
                            
              
                       
                        </div> <!-- /.row -->
                    </div> <!-- /.container -->
                </div>
                <!-- End rotate box-1 -->
                
                <div class="extra-space-l"></div>
                
              
          </section>
          <!-- End about section -->

                      
              
                   
              
              
          
          <section id="cta-section">
          	<div class="cta">
            	<div class="container">
                	<div class="row">
                    
                    	<div class="col-md-9">
                        	<h1>Paper</h1>
                            <p>Effective Online Software Anomaly Detection 
Yizhen Chen, Ming Ying, Daren Liu, Adil Alim, Feng Chen, and Mei-Hwa Chen </p><p>
in Proceedings of the ACM SIGSOFT International Symposium on Software Testing and Analysis (ISSTA'17), 2017. </p>
                        </div>
                        
                        <div class="col-md-3">
                        	<div class="cta-btn" >
                        <a class="btn btn-default btn-lg" target="_blank" href="resource/Automatic_Online_Software_Anomaly_Detection-2.pdf" role="button">Download Paper</a>
                        	</div>
                        	
                        	<div class="cta-btn" >
                        <a class="btn btn-default btn-lg"  href="invariantselection.jsp" target="_blank" role="button">Try A Demo</a>
                        	</div>
                        </div>
                        
                    </div>
                </div> 
            </div>
          </section>
      

              
      
              
              
           <section id="services-section" class="page text-center">
     
                <div class="page-header-wrapper">
                    <div class="container">
                        <div class="page-header text-center" >
                            <h2>OUR TEAM</h2>
                            <div class="devider"></div>
                         
                        </div>
                    </div>
                </div>
         
                <div class="rotate-box-2-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <a href="#" class="rotate-box-2 square-icon text-center " data-wow-delay="0">
                                    <span class="rotate-box-icon"><i class="fa fa-mobile"></i></span>
                                    <div class="rotate-box-info">
                                        <h4>Mei-Hwa Chen</h4>
                                        <p>Professor </p>
                                        <p>Department of Informatics, College of Engineering and Applied Sciences at University of albany</p>
                                        <p>Research Interests: Software reliability modeling, software testing, software architecture</p> 
                                        
                                    </div>
                                </a>
                            </div>
            
                            <div class="col-md-4 col-sm-6">
                                <a href="#" class="rotate-box-2 square-icon text-center " data-wow-delay="0.2s">
                                    <span class="rotate-box-icon"><i class="fa fa-pie-chart"></i></span>
                                    <div class="rotate-box-info">
                                        <h4>Feng Chen</h4>
                                        <p>Professor </p>
                                        <p>Department of Informatics, College of Engineering and Applied Sciences at University of albany</p>
                                    	<p>Research Interests: Detection of emerging events and other relevant patterns in the mobile context and/or data mining of spatial temporal, textual, or social media data</p>
                                    </div>
                                </a>
                            </div>
            
                            <div class="col-md-4 col-sm-6">
                                <a href="#" class="rotate-box-2 square-icon text-center " data-wow-delay="0.4s">
                                    <span class="rotate-box-icon"><i class="fa fa-cloud"></i></span>
                                    <div class="rotate-box-info">
                                        <h4>Yizhen Chen</h4>
                                        <p>PhD candidate at computer science department of university of albany</p>
                                        <p>1215 Western Ave,UAB 403, Albany,NY 1222 USA</p>
                                    </div>
                                </a>
                            </div>
                            
        
                            
                        </div> 
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <a href="#" class="rotate-box-2 square-icon text-center " data-wow-delay="0">
                                    <span class="rotate-box-icon"><i class="fa fa-mobile"></i></span>
                                    <div class="rotate-box-info">
                                        <h4>Yin Ming</h4>
                                        <p>PhD candidate at computer science department of university of albany</p>
                                        <p>1215 Western Ave,UAB 403, Albany,NY 1222 USA</p>
                                    </div>
                                </a>
                            </div>
            
                           
            
                            <div class="col-md-4 col-sm-6">
                                <a href="#" class="rotate-box-2 square-icon text-center " data-wow-delay="0.4s">
                                    <span class="rotate-box-icon"><i class="fa fa-cloud"></i></span>
                                    <div class="rotate-box-info">
                                        <h4>Adil Alim</h4>
                                        <p>PhD candidate at computer science department of university of albany</p>
                                        <p>1215 Western Ave,UAB 403, Albany,NY 1222 USA</p>
                                    </div>
                                </a>
                            </div>
                            
        
                             <div class="col-md-4 col-sm-6">
                                <a href="#" class="rotate-box-2 square-icon text-center " data-wow-delay="0.2s">
                                    <span class="rotate-box-icon"><i class="fa fa-pie-chart"></i></span>
                                    <div class="rotate-box-info">
                                        <h4>Daren Liu</h4>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div> 
                    
                               
                </div>
       
            </section>

           
    
                

                
            <!-- Begin footer -->
            <footer class="text-off-white">
                <!-- <div class="footer-top">
                	<div class="container">
                    	<div class="row " >

                            <div class="col-sm-6 col-md-4">
                            	<h4>Useful Links</h4>
                                <ul class="imp-links">
                                	<li><a href="">About</a></li>
                                	<li><a href="">Services</a></li>
                                	<li><a href="">Press</a></li>
                                	<li><a href="">Copyright</a></li>
                                	<li><a href="">Advertise</a></li>
                                	<li><a href="">Legal</a></li>
                                </ul>
                            </div>
                        
                        	<div class="col-sm-6 col-md-4">
                                <h4>Subscribe</h4>
                            	<div id="footer_signup">
                                    <div id="email">
                                        <form id="subscribe" method="POST">
                                            <input type="text" placeholder="Enter email address" name="email" id="address" data-validate="validate(required, email)"/>
                                            <button type="submit">Submit</button>
                                            <span id="result" class="section-description"></span>
                                        </form> 
                                    </div>
                                </div> 
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> 
                            </div>

                            <div class="col-sm-12 col-md-4">
                                <h4>Recent Tweets</h4>
                                <div class="single-tweet">
                                    <div class="tweet-content"><span>@Unika</span> Excepteur sint occaecat cupidatat non proident</div>
                                    <div class="tweet-date">1 Hour ago</div>
                                </div>
                                <div class="single-tweet">
                                    <div class="tweet-content"><span>@Unika</span> Excepteur sint occaecat cupidatat non proident uku shumaru</div>
                                    <div class="tweet-date">1 Hour ago</div>
                                </div>
                            </div>
                            
                        </div> 
                    </div> 
                   
                </div>  -->
                
                <div class="footer">
                    <!-- <div class="container text-center " >
                        <p class="copyright">Copyright &copy; 2015 - Designed By <a href="https://www.behance.net/poljakova" class="theme-author">Veronika Poljakova</a> &amp; Developed by <a href="http://www.imransdesign.com/" class="theme-author">Imransdesign</a></p>
                    </div> --> 
                    <div class="container text-center " >
                        <p class="copyright">Copyright &copy; 2017 </p>
                    </div> 
                </div>

            </footer>
            <!-- End footer -->

            <a href="#" class="scrolltotop"><i class="fa fa-arrow-up"></i></a> <!-- Scroll to top button -->
                                              
        </div><!-- body ends -->
        
        
        
        
        <!-- Plugins JS -->
		<script src="resource/inc/jquery/jquery-1.11.1.min.js"></script>
		<script src="resource/inc/bootstrap/js/bootstrap.min.js"></script>
		<script src="resource/inc/owl-carousel/js/owl.carousel.min.js"></script>
		<script src="resource/inc/stellar/js/jquery.stellar.min.js"></script>
		<script src="resource/inc/animations/js/wow.min.js"></script>
        <script src="resource/inc/waypoints.min.js"></script>
		<script src="resource/inc/isotope.pkgd.min.js"></script>
		<script src="resource/inc/classie.js"></script>
		<script src="resource/inc/jquery.easing.min.js"></script>
		<script src="resource/inc/jquery.counterup.min.js"></script>
		<script src="resource/inc/smoothscroll.js"></script>

		<!-- Theme JS -->
		<script src="resource/js/theme.js"></script>

    </body> 
        
            
</html>
