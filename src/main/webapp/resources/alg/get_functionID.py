import csv
import sys
import time
import argparse
import random
csv.field_size_limit(922337203)
import os
dir_path = os.path.dirname(os.path.realpath(__file__))

# fun_IDF="Data/time12/time12-fun_id.txt" #w
# funID_iF="Data/time12/time12-funID_invariant.txt" #w
# fun_InvF="Data/time12/fun_invariant.txt"   #r


def get_class_id(fun_InvF,fun_IDF,funID_iF):

    funId=open(fun_IDF,"w")
    fun_inv=open(funID_iF,"w")
    fun_list=[]
    with open(fun_InvF,"r") as fF:
        for line in fF.readlines():
            line=line.strip().split(":::")[0]
            terms=line.split()
            fun_list.append(terms[0])
    print len(fun_list)
    fun_list=list(set(fun_list))
    print len(fun_list)
    fun_id_dict={}
    for i,fun in enumerate(fun_list):
        funId.write(fun+" "+str(i)+"\n")
        fun_id_dict[fun]=i
    i=0
    with open(fun_InvF,"r") as fF:
        for line in fF.readlines():
            line=line.strip().split(":::")[0]
            line=line.strip()
            terms = line.split(" ")
            #print line,terms
            #fun_inv.write(str(fun_id_dict[terms[0]])+" "+ terms[1]+"\n")
            fun_inv.write(str(fun_id_dict[terms[0]])+" "+ str(i)+"\n")
            i+=1
    
    funId.close()
    fun_inv.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Get function id file")
    parser.add_argument('ci')
    args = parser.parse_args()
    print args.ci
    fun_IDF = dir_path+"/Data/"+str(time.strftime("%d%m%Y%I%M%S"))+str(random.randint(1, 10))+"_fun_id.txt"
    funID_iF = dir_path+"/Data/" + str(time.strftime("%d%m%Y%I%M%S")) + str(random.randint(1, 10)) + "_funID_invariant.txt"
    print "Output file 1 @" + fun_IDF
    print " @ Output file 2 @" + funID_iF
    get_class_id(args.ci, fun_IDF, funID_iF)