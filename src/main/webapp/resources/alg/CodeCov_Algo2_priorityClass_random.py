""" Priority Class Invarian Choose"""
import FC_Score_Algo2
import numpy as np
import time
import random

dataFile = "Data/time11/time11-50.txt"  #r
class_idF= "Data/time11/class_id.txt" #r
class_invF="Data/time11/class_invariant.txt" #r
class_classF="Data/time11/classRelationShip.txt" #r

class_idF= "Data/time12/class_id.txt" #r
class_invF="Data/time12/class_invariant.txt" #r
class_classF="Data/time12/classRelationShip.txt" #r


class_idF= "Data/math/class_id.txt" #r
class_invF="Data/math/math_class_invariant.txt" #r
class_classF="Data/math/classRelationShip.txt" #r
paths=["Data/math/mathNewToOldest/math-","Data/math/mathOldToNewest/math-","Data/math/mathRandom/math-"]

class_idF= "Data/Collection_class_id.txt" #r
class_invF="Data/collection-class-invariant.txt" #r
class_classF="Data/CollectionNewData/classRelationShip.txt" #r
paths=["Data/collectionNewToOldest/collection-","Data/collectionOldToNewest/collection-","Data/collectionRandom/collection-"]

class_classF="Data/CollectionNewData/classRelationShip.txt" #r
class_idF= "Data/Collection_class_id.txt" #r
paths=["Data/collection_codecoverage/time15-"]

class_classF="Data/math/classRelationShip.txt" #r
class_idF= "Data/math/class_id.txt" #r
paths=["Data/math/codecoverage/math-"]

class_classF="Data/CollectionNewData/classRelationShip.txt" #r
class_idF= "Data/Collection_class_id.txt" #r
paths=["Data/collection_codecoverage/collection-"]

class_classF="Data/math/classRelationShip.txt" #r
class_idF= "Data/math/class_id.txt" #r
paths=["Data/math/codecoverage/math-"]

dd=0
rf=open(paths[dd]+"_Algo-2a-random-result"+paths[dd].split("/")[2]+".txt","a+")  
for k in [10,20,30,40,50,60,70,80,90]:#]:#[10,20,30,40,50,60,70,80,90]:
    
    class_invF=paths[dd]+str(k)+"-class-invariant.txt" #r
    
    dataFile = paths[dd]+str(k)+".txt" #r
    for runTime in range(1):
        rf.write(">>>>>>>>>>>>>>>>>>----- "+str(runTime)+" -----<<<<<<<<<<<<<<<<<<<\n")
        print ">>>>>>>>>>>>>>>>>>----- ",runTime," -----<<<<<<<<<<<<<<<<<<<" 
        t0=time.time()
        class_id_dict = {}
        with open(class_idF, "r") as c_idF:
            for line in c_idF.readlines():
                e = line.replace("\n", "").split(" ")
                class_id_dict[e[1].replace("\r", "")] = int(e[0])
        N = len(class_id_dict)
        
   
        class_invar_List = [[] for i in range(N)]
        
        class_numb=len(class_id_dict)
        inv_numb=0
        with open(class_invF, "r") as c_i_F:
            for index, line in enumerate(c_i_F.readlines()):
                #print ">>",line
                inv_numb+=1
                if class_id_dict.has_key(line.replace("\n", "")):
                    class_invar_List[class_id_dict[line.replace("\n", "")]].append(index)
        
        edges = []
        with open(class_classF, "r") as c_c_F:
            for line in c_c_F.readlines():
                e = line.replace("\n", "").split()
                if e[0] != e[1] and class_id_dict.has_key(e[0]) and class_id_dict.has_key(e[1]):
                    edges.append((class_id_dict[e[0]], class_id_dict[e[1]]))
        #print len(edges)
        
        adjList = np.zeros((N, N))
        for e in edges:
            adjList[e[0]][e[1]] = 1
            adjList[e[1]][e[0]] = 1
            # pprint.pprint(adjList)
        # pprint.pprint(np.sum(adjList,axis=1))
        B_I = inv_numb*0.001
        B_C = int(class_numb*0.3)
        #print "B_I and B_C= ",class_numb,B_C,B_I
        y,V,d=FC_Score_Algo2.readData1(dataFile)
        
        S_star = []
        C_star = []
        for i, c in enumerate(class_invar_List):
            for s in S_star:
                if s in c:
                    C_star.append(i)
        C_star = list(set(C_star))
        
        #print "C_star: ",C_star
        sorted_degree = []
        for i, deg in enumerate(adjList):
            sorted_degree.append((i, sum(deg)))
        # print sorted_degree
        sorted_degree = sorted(sorted_degree, key=lambda tup: tup[1], reverse=True)
        #print sorted_degree
        for degree in sorted_degree:
            #if len(C_star) <= B_C and degree[0] not in C_star and degree[0] in pri_class_id:
            if len(C_star) <= B_C and len(S_star)<200 and degree[0] not in C_star:
                if len(class_invar_List[degree[0]])>0:
                    C_star.append(degree[0])
                    S_star.append(random.choice(class_invar_List[degree[0]]))
            else: 
                continue
        
        print dataFile,"============================"
        print "Invariant S_star ",len(S_star), S_star
        rf.write(dataFile+"============================\n")
        rf.write(",".join(map(str,S_star))+"\n")
rf.close()    